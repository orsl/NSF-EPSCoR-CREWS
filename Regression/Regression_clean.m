%% Cleaned up version of regression analysis of UCFR CREWS data - this script loads data, corrects for water attenuation, then estimates pigment concentration

%Riley Logan
%July 2022

%% Block some annoying warnings
% Make sure this is all good at some point...
w = warning('off','all');
%rmpath('folderthatisnotonpath')

%% Define which analysis method to use
simpleRatio_on = 1;
normalizedDiff_on = 0;

%% Directories
BM20210817_dataDir = '2021/08-17/Bearmouth/';
GC20210817_dataDir = '2021/08-17/GoldCreek/';
GC20210909_dataDir = '2021/09-09_SecondAttempt/';
GAR20220628_dataDir = '2022/06-28';
DL20220629_dataDir = '2022/06-29';
GAR20220712_dataDir = '2022/07-12';
GC20220713_dataDir = '2022/07-13';
GC20220726_dataDir = '2022/07-26';
BG20220727_dataDir = '2022/07-27';

Biomass_info = readtable('Biomass_Data/z_compiledWorkbook_currentAsOf_2022-07-25_withZeroINsteadof9999.xlsx');
updatedBiomass = readtable('Biomass_Data/INTEGRATED_3RD_ROUND_CAO_2022-08-10.xlsx');

%% Read in hoop reflectance data
% Band band removal metric - remove wavelengths above ~870 nm
badBands = 79;
numberWaves2021 = 300-badBands;
badBands2022 = 59;
numberWaves2022 = 300-badBands;

% Bearmouth 2021-08-17 data
BM20210817_dir = dir(fullfile(BM20210817_dataDir,'*.txt'));
BM20210817_hoopReflectance = zeros(numberWaves2021,size(BM20210817_dir,1)-1);
for k = 1:size(BM20210817_dir,1)-1
    T = readtable(fullfile(BM20210817_dir(k).folder, BM20210817_dir(k).name));
    BM20210817_hoopReflectance(:,k) = table2array(T(1:end-badBands,2));
end

% Gold Creek 2021-08-17 data
GC20210817_dir = dir(fullfile(GC20210817_dataDir,'*.txt'));
GC20210817_hoopReflectance = zeros(numberWaves2021,size(GC20210817_dir,1)-1);
for k = 1:size(GC20210817_dir,1)-1
    T = readtable(fullfile(GC20210817_dir(k).folder, GC20210817_dir(k).name));
    GC20210817_hoopReflectance(:,k) = table2array(T(1:end-badBands,2));
end

% Gold Creek 2021-09-09 data
GC20210909_dir = dir(fullfile(GC20210909_dataDir,'*.txt'));
GC20210909_hoopReflectance = zeros(numberWaves2021,size(GC20210909_dir,1));
for k = 1:size(GC20210909_dir,1)
    T = readtable(fullfile(GC20210909_dir(k).folder, GC20210909_dir(k).name));
    GC20210909_hoopReflectance(:,k) = table2array(T(1:end-badBands,2));
end

% Garrison 2022-06-28 data
GAR20220628_dir = dir(fullfile(GAR20220628_dataDir,'*.txt'));
GAR20220628_hoopReflectance = zeros(numberWaves2022,size(GAR20220628_dir,1)-1);
for k = 1:size(GAR20220628_dir,1)
    T = readtable(fullfile(GAR20220628_dir(k).folder, GAR20220628_dir(k).name));
    GAR20220628_hoopReflectance(:,k) = table2array(T(1:end-badBands2022,2));
end

% Hoop was split between cubes - average the pixels from each image into a
% single array, then replace
ave = (GAR20220628_hoopReflectance(:,9) + GAR20220628_hoopReflectance(:,10)) / 2;
GAR20220628_hoopReflectance(:,9) = ave;
GAR20220628_hoopReflectance(:,10) = [];

% Deer Lodge 2022-06-29 data
DL20220629_dir = dir(fullfile(DL20220629_dataDir,'*.csv'));
DL20220629_hoopReflectance = zeros(numberWaves2022,size(DL20220629_dir,1));
for k = 1:size(DL20220629_dir,1)
    T = readtable(fullfile(DL20220629_dir(k).folder, DL20220629_dir(k).name));
    DL20220629_hoopReflectance(:,k) = table2array(T(1:end-badBands2022,2));
end

% Garrison 2022-07-12 data
GAR20220712_dir = dir(fullfile(GAR20220712_dataDir,'*.csv'));
GAR20220712_hoopReflectance = zeros(numberWaves2022,size(DL20220629_dir,1));
for k = 1:size(GAR20220712_dir,1)
    T = readtable(fullfile(GAR20220712_dir(k).folder, GAR20220712_dir(k).name));
    GAR20220712_hoopReflectance(:,k) = table2array(T(1:end-badBands2022,2));
end

% Gold Creek 2022-07-13 data
GC20220713_dir = dir(fullfile(GC20220713_dataDir,'*.csv'));
GC20220713_hoopReflectance = zeros(numberWaves2022,size(GC20220713_dir,1));
for k = 1:size(GC20220713_dir,1)
    T = readtable(fullfile(GC20220713_dir(k).folder, GC20220713_dir(k).name));
    GC20220713_hoopReflectance(:,k) = table2array(T(1:end-32,2));
end

% Gold Creek 2022-07-26 data
GC20220726_dir = dir(fullfile(GC20220726_dataDir,'*.csv'));
GC20220726_hoopReflectance = zeros(numberWaves2022,size(GC20220726_dir,1));
for k = 1:size(GC20220726_dir,1)
    T = readtable(fullfile(GC20220726_dir(k).folder, GC20220726_dir(k).name));
    GC20220726_hoopReflectance(:,k) = table2array(T(1:end,2));
end

% Bear Gulch 2022-07-27 data
BG20220727_dir = dir(fullfile(BG20220727_dataDir,'*.csv'));
BG20220727_hoopReflectance = zeros(numberWaves2022,size(BG20220727_dir,1));
for k = 1:size(BG20220727_dir,1)
    T = readtable(fullfile(BG20220727_dir(k).folder, BG20220727_dir(k).name));
    BG20220727_hoopReflectance(:,k) = table2array(T(1:end,2));
end

%% Define wavelength vector
wavelengths = readtable(fullfile(BM20210817_dir(1).folder, BM20210817_dir(1).name));
wavelengths = wavelengths.Var1(1:end-badBands);

clear BM20210817_dir GC20210817_dir GC20210909_dir GAR20220628_dir DL20220629_dir GAR20220712_dir T...
    GC20220713_dir GC20220726_dir BG20220727_dir

%% Read in depth data
GC20210817_hoopDepths = Biomass_info.AVERAGE_DEPTH(1:7);
BM20210817_hoopDepths = Biomass_info.AVERAGE_DEPTH(8:14);
GC20210909_hoopDepths = Biomass_info.AVERAGE_DEPTH(15:34);
GAR20220628_hoopDepths = Biomass_info.AVERAGE_DEPTH(35:50);
DL20220629_hoopDepths = Biomass_info.AVERAGE_DEPTH(51:70);
GAR20220712_hoopDepths = Biomass_info.AVERAGE_DEPTH(71:91);
GC20220713_hoopDepths = Biomass_info.AVERAGE_DEPTH(92:112);

%% Combine data sets
%Only 2021 Gold Creek
%GC_allHoops = [GC20210817_hoopReflectance, GC20210909_hoopReflectance];
%GC_depths = [GC20210817_hoopDepths', GC20210909_hoopDepths'];

% All hoops
allHoops = [GC20210817_hoopReflectance, BM20210817_hoopReflectance, GC20210909_hoopReflectance, ...
    GAR20220628_hoopReflectance, DL20220629_hoopReflectance, GAR20220712_hoopReflectance, GC20220713_hoopReflectance, GC20220726_hoopReflectance, BG20220727_hoopReflectance];

allHoops_depths = updatedBiomass.AVERAGE_DEPTH';

%allHoops_depths = [GC20210817_hoopDepths', BM20210817_hoopDepths', GC20210909_hoopDepths', ...
%    GAR20220628_hoopDepths', DL20220629_hoopDepths', GAR20220712_hoopDepths', GC20220713_hoopDepths'];

%% Correct for water attenuation - INPUT DATA SET YOU ARE ANALYZING
waterPathLoss = waterAttenuation(allHoops_depths);

allHoops_waterCorrected = zeros(size(wavelengths,1), size(allHoops,2));
for m = 1:size(waterPathLoss,2)-1
    waterPathLoss_index = interp1(waterPathLoss(:,1), waterPathLoss(:,m+1), wavelengths);
    allHoops_waterCorrected(:,m) = allHoops(:,m) ./ (waterPathLoss_index.*0.12);
end

%% Define 2021 data
hoopData2021 = allHoops_waterCorrected(:,1:34);
pigments2021 = updatedBiomass.FILA_CHLA_MG_M2_HAUER(1:34) + updatedBiomass.EPIP_CHLA_MG_M2_HAUER(1:34);

clear allHoops

%% Load relevant biomass data

gunkIndex_sheet = updatedBiomass.GUNK_INDEX;
gunkIndex_index = gunkIndex_sheet < 0.15;

allHoops_waterCorrected_lowGunk = allHoops_waterCorrected(:,gunkIndex_index);
chla_lowGunk = updatedBiomass.TOTAL_CHL_DRONE(gunkIndex_index)';

% Old method below.... Changed Excel Workbook to make loading data
% easier (above)...


% GC20210817_totalChla = 1:7;
% for m = 1:7
%     GC20210817_totalChla(m) = Biomass_info.FILA_CHLA_MG_M2_RITCHIE(m) + Biomass_info.EPIP_CHLA_MG_M2_RITCHIE(m) + Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m);
% end
%
% GC20210817_chla_filaEpip = 1:7;
% for m = 1:7
%     GC20210817_chla_filaEpip(m) = Biomass_info.FILA_CHLA_MG_M2_RITCHIE(m) + Biomass_info.EPIP_CHLA_MG_M2_RITCHIE(m);% + Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m) + Biomass_info.FBOM_CHLA_MG_M2_RITCHIE(m);
% end
%
% GC20210817_chla_epil = 1:7;
% for m = 1:7
%     GC20210817_chla_epil(m) = Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m);
% end
%
% GC20210817_totalPhyco = 1:7;
% for m = 1:7
%     GC20210817_totalPhyco(m) = Biomass_info.PHICOCYANIN_MG_M2(m) + Biomass_info.EPIP_PHICOCYANIN_MG_M2(m); %Biomass_info.FBOM_PHICOCYANIN_MG_M2(m) +
% end
%
% GC20210817_phyco_EPIL = 1:7;
% for m = 1:7
%     GC20210817_phyco_EPIL(m) = Biomass_info.PHICOCYANIN_MG_M2(m);
% end
%
% GC20210817_phyco_EPIP = 1:7;
% for m = 1:7
%     GC20210817_phyco_EPIP(m) = Biomass_info.EPIP_PHICOCYANIN_MG_M2(m);
% end
%
% BM20210817_totalChla = 1:7;
% for m = 1:7
%     BM20210817_totalChla(m) = Biomass_info.FILA_CHLA_MG_M2_RITCHIE(m+7) + Biomass_info.EPIP_PHICOCYANIN_MG_M2(m+7) + Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m+7);% + Biomass_info.EPIP_CHLA_MG_M2_RITCHIE(m+7) + Biomass_info.FBOM_CHLA_MG_M2_RITCHIE(m+7);
% end
%
% BM20210817_chla_filaEpip = 1:7;
% for m = 1:7
%     BM20210817_chla_filaEpip(m) = Biomass_info.FILA_CHLA_MG_M2_RITCHIE(m+7)  + Biomass_info.EPIP_CHLA_MG_M2_RITCHIE(m+7);% + Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m+7) + Biomass_info.EPIP_CHLA_MG_M2_RITCHIE(m+7) + Biomass_info.FBOM_CHLA_MG_M2_RITCHIE(m+7);
% end
%
% BM20210817_chla_epil = 1:7;
% for m = 1:7
%     BM20210817_chla_epil(m) = Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m+7);
% end
%
% BM20210817_totalPhyco = 1:7;
% for m = 1:7
%     BM20210817_totalPhyco(m) = Biomass_info.ALL_PHYCOCYANIN(m+7); %Biomass_info.FBOM_PHICOCYANIN_MG_M2(m+7)
% end
%
% BM20210817_phyco_EPIP = 1:7;
% for m = 1:7
%     BM20210817_phyco_EPIP(m) = Biomass_info.EPIP_PHICOCYANIN_MG_M2(m+7);
% end
%
% BM20210817_phyco_EPIL = 1:7;
% for m = 1:7
%     BM20210817_phyco_EPIL(m) = Biomass_info.PHICOCYANIN_MG_M2(m+7);
% end
%
% GC20210909_totalChla_ritchie = 1:20;
% for m = 1:20
%     GC20210909_totalChla_ritchie(m) = Biomass_info.FILA_CHLA_MG_M2_RITCHIE(m+14) + Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m+14) + Biomass_info.EPIP_CHLA_MG_M2_RITCHIE(m+14);
% end
%
% GC20210909_chla_filaEpip = 1:20;
% for m = 1:20
%     GC20210909_chla_filaEpip(m) = Biomass_info.FILA_CHLA_MG_M2_HAUER(m+14)  + Biomass_info.EPIP_CHLA_MG_M2_HAUER(m+14);% + Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m+14) + Biomass_info.EPIP_CHLA_MG_M2_RITCHIE(m+14);
% end
%
% GC20210909_chla_epil = 1:20;
% for m = 1:20
%     GC20210909_chla_epil(m) = Biomass_info.EPIL_CHLA_MG_M2_HAUER(m+14);
% end
%
% GC20210909_phyco_EPIP = 1:20;
% for m = 1:20
%     GC20210909_phyco_EPIP(m) = Biomass_info.EPIP_PHICOCYANIN_MG_M2(m+14);
% end
%
% GC20210909_phyco_EPIL = 1:20;
% for m = 1:20
%     GC20210909_phyco_EPIL(m) = Biomass_info.PHICOCYANIN_MG_M2(m+14);
% end
%
% GC20210909_totalPhyco = 1:20;
% for m = 1:20
%     GC20210909_totalPhyco(m) = Biomass_info.PHICOCYANIN_MG_M2(m+14) + Biomass_info.EPIP_PHICOCYANIN_MG_M2(m+14);
% end
%
% GAR20220628_totalChla = 1:size(GAR20220628_hoopReflectance,2);
% for m = 1:size(GAR20220628_hoopReflectance,2)
%     GAR20220628_totalChla(m) = Biomass_info.FILA_CHLA_MG_M2_HAUER(m+34) + Biomass_info.EPIL_CHLA_MG_M2_HAUER(m+34) + Biomass_info.EPIP_CHLA_MG_M2_HAUER(m+34);
% end
%
% GAR20220628_chla_filaEpip = 1:size(GAR20220628_hoopReflectance,2);
% for m = 1:size(GAR20220628_hoopReflectance,2)
%     GAR20220628_chla_filaEpip(m) = Biomass_info.FILA_CHLA_MG_M2_HAUER(m+34) + Biomass_info.EPIP_CHLA_MG_M2_HAUER(m+34);
% end
%
% GAR20220628_chla_epil = 1:size(GAR20220628_hoopReflectance,2);
% for m = 1:size(GAR20220628_hoopReflectance,2)
%     GAR20220628_chla_epil(m) = Biomass_info.EPIL_CHLA_MG_M2_HAUER(m+34);
% end
%
% DL20220629_totalChla = 1:size(DL20220629_hoopReflectance,2);
% for m = 1:size(DL20220629_hoopReflectance,2)
%     DL20220629_totalChla(m) = Biomass_info.FILA_CHLA_MG_M2_HAUER(m+50) + Biomass_info.EPIL_CHLA_MG_M2_HAUER(m+50) + Biomass_info.EPIP_CHLA_MG_M2_HAUER(m+50);
% end
%
% DL20220629_chla_filaEpip = 1:size(DL20220629_hoopReflectance,2);
% for m = 1:size(DL20220629_hoopReflectance,2)
%     DL20220629_chla_filaEpip(m) = Biomass_info.FILA_CHLA_MG_M2_HAUER(m+50) + Biomass_info.EPIP_CHLA_MG_M2_HAUER(m+50);
% end
%
% DL20220629_chla_epil = 1:size(DL20220629_hoopReflectance,2);
% for m = 1:size(GAR20220628_hoopReflectance,2)
%     DL20220629_chla_epil(m) = Biomass_info.EPIL_CHLA_MG_M2_HAUER(m+50);
% end
%
% GAR20220712_totalChla = 1:size(GAR20220712_hoopReflectance,2);
% for m = 1:size(GAR20220712_hoopReflectance,2)
%     GAR20220712_totalChla(m) = Biomass_info.FILA_CHLA_MG_M2_HAUER(m+70) + Biomass_info.EPIL_CHLA_MG_M2_HAUER(m+70) + Biomass_info.EPIP_CHLA_MG_M2_HAUER(m+70);
% end
%
% GAR20220712_chla_filaEpip = 1:size(GAR20220712_hoopReflectance,2);
% for m = 1:size(GAR20220712_hoopReflectance,2)
%     GAR20220712_chla_filaEpip(m) = Biomass_info.FILA_CHLA_MG_M2_HAUER(m+70) + Biomass_info.EPIP_CHLA_MG_M2_HAUER(m+70);
% end
%
% GAR20220712_chla_epil = 1:size(GAR20220712_hoopReflectance,2);
% for m = 1:size(GAR20220628_hoopReflectance,2)
%     GAR20220712_chla_epil(m) = Biomass_info.EPIL_CHLA_MG_M2_HAUER(m+70);
% end
%
% GC20220713_totalChla = 1:size(GC20220713_hoopReflectance,2);
% for m = 1:size(GC20220713_hoopReflectance,2)
%     GC20220713_totalChla(m) = Biomass_info.FILA_CHLA_MG_M2_HAUER(m+91) + Biomass_info.EPIL_CHLA_MG_M2_HAUER(m+91) + Biomass_info.EPIP_CHLA_MG_M2_HAUER(m+91);
% end
%
% GC20220713_chla_filaEpip = 1:size(GC20220713_hoopReflectance,2);
% for m = 1:size(GC20220713_hoopReflectance,2)
%     GC20220713_chla_filaEpip(m) = Biomass_info.FILA_CHLA_MG_M2_HAUER(m+91) + Biomass_info.EPIP_CHLA_MG_M2_HAUER(m+91);
% end
%
% GC20220713_chla_epil = 1:size(GC20220713_hoopReflectance,2);
% for m = 1:size(GAR20220628_hoopReflectance,2)
%     GC20220713_chla_epil(m) = Biomass_info.EPIL_CHLA_MG_M2_HAUER(m+91);
% end

%% Combine data sets
%GC_chla_total = [GC20210817_totalChla, GC20210909_totalChla_ritchie];
%GC_chla_filaEpip = [GC20210817_chla_filaEpip, GC20210909_chla_filaEpip];
%GC_chla_epil = [GC20210817_chla_epil, GC20210909_chla_epil];

% total_chla_filaEpip = [GC20210817_chla_filaEpip, BM20210817_chla_filaEpip, GC20210909_chla_filaEpip,...
%     GAR20220628_chla_filaEpip, DL20220629_chla_filaEpip, GAR20220712_chla_filaEpip, GC20220713_chla_filaEpip];
%
% total_chla_epil = [GC20210817_chla_epil, BM20210817_chla_epil, GC20210909_chla_epil,...
%     GAR20220628_chla_epil, DL20220629_chla_epil, GAR20220712_chla_epil, GC20220713_chla_epil];
%
% total_chla = total_chla_filaEpip + total_chla_epil;

%% Play with removing zero points
% zeroPoints_filaEpip = find(total_chla_filaEpip == 0);
% zeroPoints_epil = find(total_chla_epil == 0);
%
% total_chla_filaEpip_noZero = total_chla_filaEpip;
% total_chla_filaEpip_noZero(zeroPoints_filaEpip) = [];
%
% total_chla_epil_noZero = total_chla_epil;
% total_chla_epil_noZero(zeroPoints_epil) = [];
%
% allHoops_waterCorrected_noZeroFilaEpip = allHoops_waterCorrected;
% allHoops_waterCorrected_noZeroFilaEpip(:,zeroPoints_filaEpip) = [];
%
% allHoops_waterCorrected_noZeroEpil = allHoops_waterCorrected;
% allHoops_waterCorrected_noZeroEpil(:,zeroPoints_epil) = [];

%% Choose variables to analyze below
pigment2analyze = updatedBiomass.FILA_CHLA_MG_M2_HAUER(find(contains(updatedBiomass.DESCRIPTOR, 'CLADOPHORA')))';
pigment2analyze = pigment2analyze(pigment2analyze<110);
hoopData = allHoops_waterCorrected(:,find(contains(updatedBiomass.DESCRIPTOR, 'CLADOPHORA')));
hoopData = hoopData(:,pigment2analyze<110);

%% Perform regression using in-situ data and the simple ratios built above

if simpleRatio_on == 1
    
    %% Build all possible simple ratios
    for m = 1:size(wavelengths,1)
        for n = 1:size(wavelengths,1)
            simpleRatios(n).num(m).den = hoopData(n,:)./hoopData(m,:);
        end
        fprintf('Building simple ratios for complete data: %.0f of %.0f \n', m, size(wavelengths,1))
    end
    
    Rsquare_linear = 0;
    Rsquare_quad = 0;
    Rsquare_exp = 0;
    Rsquare_power = 0;
    errorLinear = 100000;
    RsquareLinearFull = array2table(zeros(size(nchoosek(wavelengths,2),1),3), "VariableNames",{'R2', 'Wavelength1', 'Wavelength2'});
    Rsquare_test = 0;
    r = 1;
    w1 = 1;
    w2 = 1;
    for n = 1:size(wavelengths,1)
        simpleRatios(n).LM(m).wavelength = wavelengths(n);
        for m = 1:size(wavelengths,1)
            simpleRatios(n).LM(m).wavelength = wavelengths(m);
            [~,~,~,~,stats] = regress(pigment2analyze', [ones(size(simpleRatios(n).num(m).den')) simpleRatios(n).num(m).den']);
            
            Rsquare_test = stats(1);
            RsquareLinearFull(r, 'R2') = {stats(1)};
            r = r+1;
            RsquareLinearFull(w1, 'Wavelength1') = {wavelengths(n)};
            RsquareLinearFull(w2, 'Wavelength2') = {wavelengths(m)};
            w1 = w1+1;
            w2 = w2+1;
            %errorTest = stats(end);
            if Rsquare_test > Rsquare_linear
                Rsquare_linear = Rsquare_test;
                RsquareLinear_wave1 = wavelengths(n);
                RsquareLinear_wave2 = wavelengths(m);
            else
            end
        end
        fprintf('Analyzing complete data: %.0f of %.0f \n', n, size(wavelengths,1))
    end
    RsquareLinearFull = sortrows(RsquareLinearFull);
    
    %% Create plots from regression analysis above
    
    linearRatio = hoopData(find(wavelengths==RsquareLinear_wave1),:)./hoopData(find(wavelengths==RsquareLinear_wave2),:);
    b = regress(pigment2analyze', [ones(size(linearRatio')) linearRatio']);
    
    disp('  ')
    disp('Linear Fit')
    disp(['     Maximum R-squared: ' num2str(Rsquare_linear)])
    disp(['     Optimal band ratio: (' num2str(RsquareLinear_wave1) '/' num2str(RsquareLinear_wave2) ')'])
    disp(['     X vs. d relation: d = ' num2str(b(1),4) ' + ' ...
        num2str(b(2),4) 'X'])
    
    chlaPrediction = b(1)+b(2) .* linearRatio;
    
    rmse = sqrt(mean((pigment2analyze - chlaPrediction).^2))
    
else
end

% Create plots from regression analysis above
% Plot
figure%('position',[50 50 1600 1000])

plot(linearRatio, pigment2analyze, 'o');
%plot(linearRatio(1:7), total_chla_filaEpip(1:7),'o', linearRatio(8:end), GC_chla_filaEpip(8:end),'o', 'linewidth', 2);

axLim = axis;
hold on
plot(axLim(1:2),b(1)+b(2)*axLim(1:2),'k-','linewidth',3);
axis(axLim);
text(0.1,0.9,char(['d = ' num2str(b(1),4) ' + ' num2str(b(2),4) 'X '],...
    ['R^2 = ' num2str(Rsquare_linear,4) '; RMSE = ' num2str(rmse,4) ' mg/m^2']),...
    'units','normalized','fontname','arial','fontsize',12)
xlabel(['Band Ratio (' num2str(RsquareLinear_wave1,3) '/' num2str(RsquareLinear_wave2,3) ')'])
ylabel('Chl-a Concentration [mg/m^2]')
title('Chl-a Concentation: Linear Model')
hold off

%% Build all possible normalized difference ratios
if normalizedDiff_on == 1
    
    Rsquare_linear = 0;
    Rsquare_quad = 0;
    Rsquare_exp = 0;
    Rsquare_power = 0;
    errorLinear = 100000;
    RsquareLinearFull = array2table(zeros(size(nchoosek(wavelengths,2),1),3), "VariableNames",{'R2', 'Wavelength1', 'Wavelength2'});
    Rsquare_test = 0;
    r = 1;
    w1 = 1;
    w2 = 1;
    w3 = 1;
    w4 = 1;
    for m = 1:size(wavelengths,1)
        for n = 1:size(wavelengths,1)
            for p = 1:size(wavelengths,1)
                for q = 1:size(wavelengths,1)
                    normalizedDiff(n).num1(m).num2(p).den1(q).den2 = ( hoopData(n,:) - hoopData(m,:) )./ ( hoopData(p,:) + hoopData(q,:) );
                    
                    [~,~,~,~,stats] = regress(pigment2analyze', [ones(size(normalizedDiff(n).num1(m).num2(p).den1(q).den2')) normalizedDiff(n).num1(m).num2(p).den1(q).den2']);
                    
                    if stats(1) > 0.2
                        Rsquare_test = stats(1);
                        RsquareLinearFull(r, 'R2') = {stats(1)};
                        r = r+1;
                        RsquareLinearFull(w1, 'Wavelength1') = {wavelengths(n)};
                        RsquareLinearFull(w2, 'Wavelength2') = {wavelengths(m)};
                        RsquareLinearFull(w3, 'Wavelength3') = {wavelengths(p)};
                        RsquareLinearFull(w4, 'Wavelength4') = {wavelengths(q)};
                        w1 = w1+1;
                        w2 = w2+1;
                        w3 = w3+1;
                        w4 = w4+1;
                        %errorTest = stats(end);
                        if Rsquare_test > Rsquare_linear
                            Rsquare_linear = Rsquare_test;
                            RsquareLinear_wave1 = wavelengths(n);
                            RsquareLinear_wave2 = wavelengths(m);
                            RsquareLinear_wave3 = wavelengths(p);
                            RsquareLinear_wave4 = wavelengths(q);
                        else
                        end
                    else
                    end
                    clear normalizedDiff(n).num1(m).num2(p).den1(q).den2
                end
            end
        end
        fprintf('Building normalized difference ratios for complete data: %.0f of %.0f \n', m, size(wavelengths,1))
    end
    
    linearRatio = ( hoopData(find(wavelengths==RsquareLinear_wave1),:) - hoopData(find(wavelengths==RsquareLinear_wave2),:) ) ./ ( hoopData(find(wavelengths==RsquareLinear_wave3),:) + hoopData(find(wavelengths==RsquareLinear_wave4),:) );
    b = regress(pigment2analyze', [ones(size(linearRatio')) linearRatio']);
    
    disp('  ')
    disp('Linear Fit')
    disp(['     Maximum R-squared: ' num2str(Rsquare_linear)])
    disp(['     Optimal band ratio: (' num2str(RsquareLinear_wave1) '-' num2str(RsquareLinear_wave2) '/' num2str(RsquareLinear_wave3) '+' num2str(RsquareLinear_wave4) ')'])
    disp(['     X vs. d relation: d = ' num2str(b(1),4) ' + ' ...
        num2str(b(2),4) 'X'])
    
    chlaPrediction = b(1)+b(2) .* linearRatio;
    
    rmse = sqrt(mean((pigment2analyze - chlaPrediction).^2))
    
else
end


%% Partial Least Squares Regression Analysis
PLSR(hoopData, pigment2analyze);








