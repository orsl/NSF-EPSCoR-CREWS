%% Open refectance spectra for biomass sampling at UCFR sites: Gold Creek (GC) and Bearmouth (BM)
% Then compare in-situ sampling of chlorophyll to reflectance bands

%Riley Logan
%September 2021

% Directories
ReflectanceDir = 'C:/Users/Riley/Documents/1_Projects/NSF CREWS/2021/Data/Algal-Reflectance';
GC0909_dataDir = 'C:/Users/Riley/Documents/1_Projects/NSF CREWS/2021/Data/Algal-Reflectance/09-09_SecondAttempt';
Biomass_info = readtable('C:/Users/Riley/Documents/1_Projects/NSF CREWS/2021/Data/Algal-Reflectance/Biomass Data/DRONE_BIOMASS_DATASET_QAQC_RFL_2021_09_15.txt');

%% Enter depth information (in meters). Make sure order matches sampling location order
depth = [0.3588, 0.3778, 0.3381, 0.4636, 0.4572, 0.4763, 0.1875, 0.3, 0.345, 0.4175, 0.4175, 0.205,...
    0.3125, 0.3488, 0.45, 0.22, 0.3134, 0.3575, 0.3575, 0.4275, 0.2388, 0.3175, 0.3375, 0.4438];

depth_full = [0.2286, 0.3588, 0.3778, 0.3381, 0.4636, 0.4572, 0.4763, 0.1875, 0.3, 0.345, 0.4175, 0.4175, 0.205,...
    0.3125, 0.3488, 0.3775, 0.45, 0.22, 0.3134, 0.3575, 0.3575, 0.4275, 0.2388, 0.3175, 0.3375, 0.4438, 0.3625];

depth_allData = [0.3588, 0.3778, 0.3381, 0.4636, 0.4572, 0.4763, 0.1875, 0.3, 0.345, 0.4175, 0.4175, 0.205,...
    0.3125, 0.3488, 0.45, 0.22, 0.3134, 0.3575, 0.3575, 0.4275, 0.2388, 0.3175, 0.3375, 0.4438,...
    0.3096, 0.3143, 0.3794, 0.3794, 0.3794, 0.4143, 0.2477];

depth_allData_noRemoval = [0.2286, 0.3588, 0.3778, 0.3381, 0.4636, 0.4572, 0.4763, 0.1875, 0.3, 0.345, 0.4175, 0.4175, 0.205,...
    0.3125, 0.3488, 0.3775, 0.45, 0.22, 0.3134, 0.3575, 0.3575, 0.4275, 0.2388, 0.3175, 0.3375, 0.4438, 0.3625,...
    0.3096, 0.3143, 0.3794, 0.3794, 0.3794, 0.4143, 0.2477];

depth_GC0817 = [0.2286, 0.3588, 0.3778, 0.3381, 0.4636, 0.4572, 0.4763];

depth_BM0817 = [0.3095625, 0.314325, 0.3794125, 0.3794125, 0.3794125, 0.4143375, 0.24765];

depth_GC0909 = [0.1875, 0.3, 0.345, 0.4175, 0.4175, 0.205,0.3125, 0.3488, 0.3775, 0.45,...
    0.22, 0.3134, 0.3575, 0.3575, 0.4275, 0.2388, 0.3175, 0.3375, 0.4438, 0.3625];

%% Adjust the values below to turn on different analysis methods for chl-a concentration
%% (i.e. put the chl-a in different groups based on concentration)
%Use this to turn on gradient analysis (1 = on)
gradientOn = 0;
%Use this to turn on sub-100 analysis
sub100_on = 0;
%Use this to turn on sub-150 analysis
tween100200_on = 0;
%Use this to turn on sub-200 analysis
sub200_on = 0;
%Use this to turn on plua-200 analysis
plus200_on = 0;
%Use this to turn on all hoops analysis (Gold Creek and Bearmouth)
allHoops_on = 1;
% Build a three band ratio
threeBand_on = 0;
% Use partial least squares regression
partialLeast = 1;

% Suspect points removed: GC_0817 point 1: 0.2286 ; GC_0909 Point 9 =
% 0.3775 ; GC_0909 point 20 = 0.3625

%% Get water path loss
waterPathLoss = waterAttenuation(depth);
waterPathLoss_full = waterAttenuation(depth_full);
waterPathLoss_allData = waterAttenuation(depth_allData);
waterPathLoss_allData_noRmvl = waterAttenuation(depth_allData_noRemoval);
waterPathLoss_BM0817 = waterAttenuation(depth_BM0817);
waterPathLoss_GC0817 = waterAttenuation(depth_GC0817);
waterPathLoss_GC0909 = waterAttenuation(depth_GC0909);

%% Block some annoying warnings
% Make sure this is all good at some point...
w = warning('off','all');
%rmpath('folderthatisnotonpath')

%% Open reflectance spectra from hoops
%08-17 data
GC_hoop1_raw = readtable(fullfile(ReflectanceDir, '08-17/GoldCreek/Hoop1_downwellingConversion.txt'));
GC_hoop2_raw = readtable(fullfile(ReflectanceDir, '08-17/GoldCreek/Hoop2_downwellingConversion.txt'));
GC_hoop3_raw = readtable(fullfile(ReflectanceDir, '08-17/GoldCreek/Hoop3_downwellingConversion.txt'));
GC_hoop4_raw = readtable(fullfile(ReflectanceDir, '08-17/GoldCreek/Hoop4_downwellingConversion.txt'));
GC_hoop5_raw = readtable(fullfile(ReflectanceDir, '08-17/GoldCreek/Hoop5_downwellingConversion.txt'));
GC_hoop6_raw = readtable(fullfile(ReflectanceDir, '08-17/GoldCreek/Hoop6_downwellingConversion.txt'));
GC_hoop7_raw = readtable(fullfile(ReflectanceDir, '08-17/GoldCreek/Hoop7_downwellingConversion.txt'));

%09-09 data
GC_hoop8_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop01.txt'));
GC_hoop9_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop02.txt'));
GC_hoop10_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop03.txt'));
GC_hoop11_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop04.txt'));
GC_hoop12_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop05.txt'));
GC_hoop13_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop06.txt'));
GC_hoop14_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop07.txt'));
GC_hoop15_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop08.txt'));
GC_hoop16_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop09.txt'));
GC_hoop17_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop10.txt'));
GC_hoop18_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop11.txt'));
GC_hoop19_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop12.txt'));
GC_hoop20_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop13.txt'));
GC_hoop21_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop14.txt'));
GC_hoop22_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop15.txt'));
GC_hoop23_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop16.txt'));
GC_hoop24_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop17.txt'));
GC_hoop25_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop18.txt'));
GC_hoop26_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop19.txt'));
GC_hoop27_raw = readtable(fullfile(GC0909_dataDir, '2021-09-09_GC_Hoop20.txt'));

start = 8;
removeBadBands = 79;

%Removed 1st point - seemed suspect. Consider adding back in
GC_hoopsReflect_0817 = [GC_hoop2_raw.Var2(start:end-removeBadBands), GC_hoop3_raw.Var2(start:end-removeBadBands), GC_hoop4_raw.Var2(start:end-removeBadBands), GC_hoop5_raw.Var2(start:end-removeBadBands),...
    GC_hoop6_raw.Var2(start:end-removeBadBands), GC_hoop7_raw.Var2(start:end-removeBadBands)];

GC_hoopsReflect_0909 = [GC_hoop8_raw.Var2(start:end-removeBadBands),...% GC_hoop9_raw.Var2(1:end-removeBadBands),...
    GC_hoop10_raw.Var2(start:end-removeBadBands),...
    GC_hoop11_raw.Var2(start:end-removeBadBands), GC_hoop12_raw.Var2(start:end-removeBadBands), GC_hoop13_raw.Var2(start:end-removeBadBands), GC_hoop14_raw.Var2(start:end-removeBadBands), GC_hoop15_raw.Var2(start:end-removeBadBands),...
    GC_hoop16_raw.Var2(start:end-removeBadBands), GC_hoop17_raw.Var2(start:end-removeBadBands), GC_hoop18_raw.Var2(start:end-removeBadBands), GC_hoop19_raw.Var2(start:end-removeBadBands), GC_hoop20_raw.Var2(start:end-removeBadBands),...
    GC_hoop21_raw.Var2(start:end-removeBadBands), GC_hoop22_raw.Var2(start:end-removeBadBands), GC_hoop23_raw.Var2(start:end-removeBadBands), GC_hoop24_raw.Var2(start:end-removeBadBands), GC_hoop25_raw.Var2(start:end-removeBadBands),...
    GC_hoop26_raw.Var2(start:end-removeBadBands)];%, GC_hoop27_raw.Var2(1:end-removeBadBands)];

% comment GC_Hoop1_0917, GC_Hoop9_0909, GC_hoop27_0909 for QAQC

%GC_hoopsReflect_0909_noZero = %[GC_hoop8_raw.Var2(1:end-removeBadBands),...% GC_hoop9_raw.Var2(1:end-removeBadBands),...
%GC_hoop10_raw.Var2(1:end-removeBadBands),...
%    GC_hoop11_raw.Var2(1:end-removeBadBands), GC_hoop12_raw.Var2(1:end-removeBadBands), ...GC_hoop13_raw.Var2(1:end-removeBadBands), 
%    GC_hoop14_raw.Var2(1:end-removeBadBands), GC_hoop15_raw.Var2(1:end-removeBadBands),...
%    GC_hoop16_raw.Var2(1:end-removeBadBands), GC_hoop17_raw.Var2(1:end-removeBadBands), GC_hoop18_raw.Var2(1:end-removeBadBands), GC_hoop19_raw.Var2(1:end-removeBadBands), GC_hoop20_raw.Var2(1:end-removeBadBands),...
%    GC_hoop21_raw.Var2(1:end-removeBadBands), GC_hoop22_raw.Var2(1:end-removeBadBands), GC_hoop23_raw.Var2(1:end-removeBadBands), GC_hoop24_raw.Var2(1:end-removeBadBands), GC_hoop25_raw.Var2(1:end-removeBadBands),...
%    GC_hoop26_raw.Var2(1:end-removeBadBands);%, GC_hoop27_raw.Var2(1:end-removeBadBands)];

GC_hoopsReflect_0817_full = [GC_hoop1_raw.Var2(start:end-removeBadBands), GC_hoop2_raw.Var2(start:end-removeBadBands), GC_hoop3_raw.Var2(start:end-removeBadBands), GC_hoop4_raw.Var2(start:end-removeBadBands), GC_hoop5_raw.Var2(start:end-removeBadBands),...
    GC_hoop6_raw.Var2(start:end-removeBadBands), GC_hoop7_raw.Var2(start:end-removeBadBands)];

GC_hoopsReflect_0909_full = [GC_hoop8_raw.Var2(start:end-removeBadBands), GC_hoop9_raw.Var2(start:end-removeBadBands), GC_hoop10_raw.Var2(start:end-removeBadBands),...
    GC_hoop11_raw.Var2(start:end-removeBadBands), GC_hoop12_raw.Var2(start:end-removeBadBands), GC_hoop13_raw.Var2(start:end-removeBadBands), GC_hoop14_raw.Var2(start:end-removeBadBands), GC_hoop15_raw.Var2(start:end-removeBadBands),...
    GC_hoop16_raw.Var2(start:end-removeBadBands), GC_hoop17_raw.Var2(start:end-removeBadBands), GC_hoop18_raw.Var2(start:end-removeBadBands), GC_hoop19_raw.Var2(start:end-removeBadBands), GC_hoop20_raw.Var2(start:end-removeBadBands),...
    GC_hoop21_raw.Var2(start:end-removeBadBands), GC_hoop22_raw.Var2(start:end-removeBadBands), GC_hoop23_raw.Var2(start:end-removeBadBands), GC_hoop24_raw.Var2(start:end-removeBadBands), GC_hoop25_raw.Var2(start:end-removeBadBands),...
    GC_hoop26_raw.Var2(start:end-removeBadBands), GC_hoop27_raw.Var2(start:end-removeBadBands)];

%GC_hoops_reflect_sub100 = [GC_hoops_reflect(:,1), GC_hoops_reflect(:,2), GC_hoops_reflect(:,8),GC_hoops_reflect(:,9),GC_hoops_reflect(:,13),GC_hoops_reflect(:,14),GC_hoops_reflect(:,16),...
%    GC_hoops_reflect(:,18),GC_hoops_reflect(:,23),GC_hoops_reflect(:,24),GC_hoops_reflect(:,27)];

%GC_hoops_reflect_sub200 = [GC_hoopsReflect_0817(:,1), GC_hoopsReflect_0817(:,2), GC_hoopsReflect_0817(:,3),GC_hoopsReflect_0817(:,4),GC_hoopsReflect_0817(:,6),GC_hoopsReflect_0909(:,1),GC_hoopsReflect_0909(:,2),...
%   GC_hoopsReflect_0909(:,5), GC_hoopsReflect_0909(:,6),GC_hoopsReflect_0909(:,7),GC_hoopsReflect_0909(:,9), GC_hoopsReflect_0909(:,10), GC_hoopsReflect_0909(:,11), GC_hoopsReflect_0909(:,12), GC_hoopsReflect_0909(:,13)...
%  GC_hoopsReflect_0909(:,16), GC_hoopsReflect_0909(:,17), GC_hoopsReflect_0909(:,20)];

% GC_hoops_reflect_sub150 = [GC_hoopsReflect_0817(:,1), GC_hoopsReflect_0817(:,2),GC_hoopsReflect_0909(:,1),GC_hoopsReflect_0909(:,2),...
%     GC_hoopsReflect_0909(:,6), GC_hoopsReflect_0909(:,7), ...GC_hoopsReflect_0909(:,9),
%     GC_hoopsReflect_0909(:,10), GC_hoopsReflect_0909(:,11),...
%     GC_hoopsReflect_0909(:,16), GC_hoopsReflect_0909(:,17)];%, GC_hoopsReflect_0909(:,20)];

wavelengths = GC_hoop1_raw.Var1(start:end-removeBadBands);

% m = 1;
% for n = 1:2:size(GC_hoop1_reflect)-1
%     GC_hoop1_reflectAve(m) = (GC_hoop1_reflect(n) + GC_hoop1_reflect(n+1))/2;
%     GC_hoop2_reflectAve(m) = (GC_hoop2_reflect(n) + GC_hoop2_reflect(n+1))/2;
%     GC_hoop3_reflectAve(m) = (GC_hoop3_reflect(n) + GC_hoop3_reflect(n+1))/2;
%     GC_hoop4_reflectAve(m) = (GC_hoop4_reflect(n) + GC_hoop4_reflect(n+1))/2;
%     GC_hoop5_reflectAve(m) = (GC_hoop5_reflect(n) + GC_hoop5_reflect(n+1))/2;
%     GC_hoop6_reflectAve(m) = (GC_hoop6_reflect(n) + GC_hoop6_reflect(n+1))/2;
%     GC_hoop7_reflectAve(m) = (GC_hoop7_reflect(n) + GC_hoop7_reflect(n+1))/2;
%     m = m+1;
% end

BM_hoop1_raw = readtable(fullfile(ReflectanceDir, '08-17/Bearmouth/Hoop1-downwelling-Correction.txt'));
BM_hoop2_raw = readtable(fullfile(ReflectanceDir, '08-17/Bearmouth/Hoop2-downwelling-Correction.txt'));
BM_hoop3_raw = readtable(fullfile(ReflectanceDir, '08-17/Bearmouth/Hoop3-downwelling-Correction.txt'));
BM_hoop4_raw = readtable(fullfile(ReflectanceDir, '08-17/Bearmouth/Hoop4-downwelling-Correction.txt'));
BM_hoop5_raw = readtable(fullfile(ReflectanceDir, '08-17/Bearmouth/Hoop5-downwelling-Correction.txt'));
BM_hoop6_raw = readtable(fullfile(ReflectanceDir, '08-17/Bearmouth/Hoop6-downwelling-Correction.txt'));
BM_hoop7_raw = readtable(fullfile(ReflectanceDir, '08-17/Bearmouth/Hoop7-downwelling-Correction.txt'));

BM_hoopsReflect_0817 = [BM_hoop1_raw.Var2(start:end-removeBadBands), BM_hoop2_raw.Var2(start:end-removeBadBands), BM_hoop3_raw.Var2(start:end-removeBadBands), BM_hoop4_raw.Var2(start:end-removeBadBands),...
    BM_hoop5_raw.Var2(start:end-removeBadBands), BM_hoop6_raw.Var2(start:end-removeBadBands), BM_hoop7_raw.Var2(start:end-removeBadBands)];

clear GC_hoop1_raw GC_hoop2_raw GC_hoop3_raw GC_hoop4_raw GC_hoop5_raw GC_hoop6_raw GC_hoop7_raw GC_hoop8_raw GC_hoop9_raw GC_hoop10_raw...
    GC_hoop11_raw GC_hoop12_raw GC_hoop13_raw GC_hoop14_raw GC_hoop15_raw GC_hoop16_raw GC_hoop17_raw GC_hoop18_raw GC_hoop19_raw GC_hoop20_raw...
    GC_hoop21_raw GC_hoop22_raw GC_hoop23_raw GC_hoop24_raw GC_hoop25_raw GC_hoop26_raw GC_hoop27_raw BM_hoop1_raw BM_hoop2_raw BM_hoop3_raw...
    BM_hoop4_raw BM_hoop5_raw BM_hoop6_raw BM_hoop7_raw

% m = 1;
% for n = 1:2:size(BM_hoop1_reflect)-1
%     BM_hoop1_reflectAve(m) = (BM_hoop1_reflect(n) + BM_hoop1_reflect(n+1))/2;
%     BM_hoop2_reflectAve(m) = (BM_hoop2_reflect(n) + BM_hoop2_reflect(n+1))/2;
%     BM_hoop3_reflectAve(m) = (BM_hoop3_reflect(n) + BM_hoop3_reflect(n+1))/2;
%     BM_hoop4_reflectAve(m) = (BM_hoop4_reflect(n) + BM_hoop4_reflect(n+1))/2;
%     BM_hoop5_reflectAve(m) = (BM_hoop5_reflect(n) + BM_hoop5_reflect(n+1))/2;
%     BM_hoop6_reflectAve(m) = (BM_hoop6_reflect(n) + BM_hoop6_reflect(n+1))/2;
%     BM_hoop7_reflectAve(m) = (BM_hoop7_reflect(n) + BM_hoop7_reflect(n+1))/2;
%     m = m+1;
% end

% Correct data for water path loss
GC_allHoops = [GC_hoopsReflect_0817, GC_hoopsReflect_0909];

GC_allHoops_waterCorrected = zeros(size(wavelengths,1), size(GC_allHoops,2));
GC_allHoops_waterCorrected_noC = zeros(size(wavelengths,1), size(GC_allHoops,2));
for m = 1:size(waterPathLoss,2)-1
    waterPathLoss_index = interp1(waterPathLoss(:,1), waterPathLoss(:,m+1), wavelengths);
    GC_allHoops_waterCorrected_noC(:,m) = GC_allHoops(:,m) ./ waterPathLoss_index;
    GC_allHoops_waterCorrected(:,m) = GC_allHoops(:,m) ./ (waterPathLoss_index.*0.12);
end

GC_allHoops_QAQC_noZero = GC_allHoops_waterCorrected;
GC_allHoops_QAQC_noZero(:,7) = [];
GC_allHoops_QAQC_noZero(:,10) = [];

%% FIX THIS!!! ONLY ONE CORRECTED!
%allHoops_noZero = [GC_allHoops_QAQC_noZero, BM_hoopsReflect_0817];
%allHoops_noZero(:,10) = [];

% Correct data for water path loss - full GC dataset 
GC_allHoops_full = [GC_hoopsReflect_0817_full, GC_hoopsReflect_0909_full];
GC_allHoops_waterCorrected_full = zeros(size(wavelengths,1), size(GC_allHoops,2));
for m = 1:size(waterPathLoss_full,2)-1
    waterPathLoss_index = interp1(waterPathLoss_full(:,1), waterPathLoss_full(:,m+1), wavelengths);
    GC_allHoops_waterCorrected_full(:,m) = GC_allHoops_full(:,m) ./ waterPathLoss_index;
end

% Correct data for water path loss - full BM dataset 
BM_allHoops_waterCorrected = zeros(size(wavelengths,1), size(BM_hoopsReflect_0817,2));
for m = 1:size(waterPathLoss_BM0817,2)-1
    waterPathLoss_index = interp1(waterPathLoss_BM0817(:,1), waterPathLoss_BM0817(:,m+1), wavelengths);
    BM_allHoops_waterCorrected(:,m) = BM_hoopsReflect_0817(:,m) ./ waterPathLoss_index;
end


% Correct data for water path loss - full dataset
allHoops = [GC_hoopsReflect_0817, GC_hoopsReflect_0909, BM_hoopsReflect_0817];

allHoops_waterCorrect = zeros(size(wavelengths,1), size(allHoops,2));
for m = 1:size(waterPathLoss_allData,2)-1
    waterPathLoss_index = interp1(waterPathLoss_allData(:,1), waterPathLoss_allData(:,m+1), wavelengths);
    allHoops_waterCorrect(:,m) = allHoops(:,m) ./ waterPathLoss_index;
end

%Correct data for water path loss - GC 08-17 
GC_0817_waterCorrect = zeros(size(wavelengths,1), size(GC_hoopsReflect_0817,2));
for m = 1:size(waterPathLoss_GC0817,2)-1
    waterPathLoss_index = interp1(waterPathLoss_GC0817(:,1), waterPathLoss_GC0817(:,m+1), wavelengths);
    GC_0817_waterCorrect(:,m) = GC_hoopsReflect_0817_full(:,m) ./ waterPathLoss_index;
end

%Correct data for water path loss - GC 09-09
GC_0909_waterCorrect = zeros(size(wavelengths,1), size(GC_hoopsReflect_0909_full,2));
for m = 1:size(waterPathLoss_GC0909,2)-1
    waterPathLoss_index = interp1(waterPathLoss_GC0909(:,1), waterPathLoss_GC0909(:,m+1), wavelengths);
    GC_0909_waterCorrect(:,m) = GC_hoopsReflect_0909_full(:,m) ./ waterPathLoss_index;
end

% Correct data for water path loss - full dataset no removals (full GC +
% BM)
if allHoops_on == 1
    allHoops_allData_noRmvl = [GC_hoopsReflect_0817_full, GC_hoopsReflect_0909_full, BM_hoopsReflect_0817];
    allHoops_waterCorrected_allData_noRmvl = zeros(size(wavelengths,1), size(allHoops_allData_noRmvl,2));
    for m = 1:size(waterPathLoss_allData_noRmvl,2)-1
        waterPathLoss_index = interp1(waterPathLoss_allData_noRmvl(:,1), waterPathLoss_allData_noRmvl(:,m+1), wavelengths);
        allHoops_waterCorrected_allData_noRmvl(:,m) = allHoops_allData_noRmvl(:,m) ./ waterPathLoss_index;
    end
else
end

% Smooth the data
%order = 2;
%framelen = 5;
%allHoops_smooth = sgolayfilt(allHoops,order,framelen);

%% load chlorophyll data

GC_08_17_chlorophyll_content = 1:7;
for m = 1:7
    GC_08_17_chlorophyll_content(m) = Biomass_info.FILA_CHLA_MG_M2_RITCHIE(m) + Biomass_info.EPIP_CHLA_MG_M2_RITCHIE(m) + Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m);
end

GC_08_17_chla_filaEpip = 1:7;
for m = 1:7
    GC_08_17_chla_filaEpip(m) = Biomass_info.FILA_CHLA_MG_M2_RITCHIE(m) + Biomass_info.EPIP_CHLA_MG_M2_RITCHIE(m);% + Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m) + Biomass_info.FBOM_CHLA_MG_M2_RITCHIE(m);
end

GC_08_17_chla_epil = 1:7;
for m = 1:7
    GC_08_17_chla_epil(m) = Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m);
end

GC_08_17_phycocyanin_content = 1:7;
for m = 1:7
    GC_08_17_phycocyanin_content(m) = Biomass_info.PHICOCYANIN_MG_M2(m) + Biomass_info.EPIP_PHICOCYANIN_MG_M2(m); %Biomass_info.FBOM_PHICOCYANIN_MG_M2(m) +
end

GC_08_17_phyco_EPIL = 1:7;
for m = 1:7
    GC_08_17_phyco_EPIL(m) = Biomass_info.PHICOCYANIN_MG_M2(m);
end

GC_08_17_phyco_EPIP = 1:7;
for m = 1:7
    GC_08_17_phyco_EPIP(m) = Biomass_info.EPIP_PHICOCYANIN_MG_M2(m);
end

BM_chlorophyll_content = 1:7;
for m = 1:7
    BM_chlorophyll_content(m) = Biomass_info.FILA_CHLA_MG_M2_RITCHIE(m+7) + Biomass_info.EPIP_PHICOCYANIN_MG_M2(m+7) + Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m+7);% + Biomass_info.EPIP_CHLA_MG_M2_RITCHIE(m+7) + Biomass_info.FBOM_CHLA_MG_M2_RITCHIE(m+7);
end

BM_chla_filaEpip = 1:7;
for m = 1:7
    BM_chla_filaEpip(m) = Biomass_info.FILA_CHLA_MG_M2_RITCHIE(m+7)  + Biomass_info.EPIP_CHLA_MG_M2_RITCHIE(m+7);% + Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m+7) + Biomass_info.EPIP_CHLA_MG_M2_RITCHIE(m+7) + Biomass_info.FBOM_CHLA_MG_M2_RITCHIE(m+7);
end

BM_chla_epil = 1:7;
for m = 1:7
    BM_chla_epil(m) = Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m+7);
end

BM_phycocyanin_content = 1:7;
for m = 1:7
    BM_phycocyanin_content(m) = Biomass_info.ALL_PHYCOCYANIN(m+7); %Biomass_info.FBOM_PHICOCYANIN_MG_M2(m+7)
end

BM_phyco_EPIP = 1:7;
for m = 1:7
    BM_phyco_EPIP(m) = Biomass_info.EPIP_PHICOCYANIN_MG_M2(m+7);
end

BM_phyco_EPIL = 1:7;
for m = 1:7
    BM_phyco_EPIL(m) = Biomass_info.PHICOCYANIN_MG_M2(m+7);
end

GC_09_09_chlorophyll_content_ritchie = 1:20;
for m = 1:20
    GC_09_09_chlorophyll_content_ritchie(m) = Biomass_info.FILA_CHLA_MG_M2_RITCHIE(m+14) + Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m+14) + Biomass_info.EPIP_CHLA_MG_M2_RITCHIE(m+14);
end

GC_09_09_chla_filaEpip = 1:20;
for m = 1:20
    GC_09_09_chla_filaEpip(m) = Biomass_info.FILA_CHLA_MG_M2_RITCHIE(m+14)  + Biomass_info.EPIP_CHLA_MG_M2_RITCHIE(m+14);% + Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m+14) + Biomass_info.EPIP_CHLA_MG_M2_RITCHIE(m+14);
end

GC_09_09_chla_epil = 1:20;
for m = 1:20
    GC_09_09_chla_epil(m) = Biomass_info.EPIL_CHLA_MG_M2_RITCHIE(m+14);
end

GC_09_09_phyco_EPIP = 1:20;
for m = 1:20
    GC_09_09_phyco_EPIP(m) = Biomass_info.EPIP_PHICOCYANIN_MG_M2(m+14);
end

GC_09_09_phyco_EPIL = 1:20;
for m = 1:20
    GC_09_09_phyco_EPIL(m) = Biomass_info.PHICOCYANIN_MG_M2(m+14);
end

GC_09_09_phycocyanin_content = 1:20;
for m = 1:20
    GC_09_09_phycocyanin_content(m) = Biomass_info.PHICOCYANIN_MG_M2(m+14) + Biomass_info.EPIP_PHICOCYANIN_MG_M2(m+14);
end

GC_total_chla_ritchie_noRmvl = [GC_08_17_chlorophyll_content, GC_09_09_chlorophyll_content_ritchie];

for m = 1:34
    totalChla_allSites(m) = Biomass_info.ALL_CHLA_MG_M2_RITCHIE(m);
end

for m = 1:34
    gunkIndex(m) = Biomass_info.EPIP_CHLA_MG_M2_RITCHIE(m) / Biomass_info.FILA_CHLA_MG_M2_RITCHIE(m);
end


GC_0817_allChla = totalChla_allSites(1:7);
BM_0817_allChla = totalChla_allSites(8:14);
GC_0909_allChla = totalChla_allSites(15:end);

GC_allChla = [GC_0817_allChla, GC_0909_allChla];
totalChla_allSites = [GC_0817_allChla, GC_0909_allChla, BM_0817_allChla];
Chla_allSites_filaEpip = [GC_08_17_chla_filaEpip, GC_09_09_chla_filaEpip, BM_chla_filaEpip];
Chla_allSites_epil = [GC_08_17_chla_epil, GC_09_09_chla_epil, BM_chla_epil];

GC_total_phycocyanin = [GC_08_17_phycocyanin_content, GC_09_09_phycocyanin_content];
GC_allPhyco_QA = [GC_08_17_phycocyanin_content(1:7), GC_09_09_phycocyanin_content(1:8) ,GC_09_09_phycocyanin_content(10:19)];

totalPhyco = [GC_total_phycocyanin, BM_phycocyanin_content];
totalPhyco_EPIP = [GC_08_17_phyco_EPIP, GC_09_09_phyco_EPIP, BM_phyco_EPIP];
totalPhyco_EPIL = [GC_08_17_phyco_EPIL, GC_09_09_phyco_EPIL, BM_phyco_EPIL];

GC_chla_total = [GC_0817_allChla, GC_0909_allChla];
GC_chla_filaEpip = [GC_08_17_chla_filaEpip, GC_09_09_chla_filaEpip];
GC_chla_epil = [GC_08_17_chla_epil, GC_09_09_chla_epil];
GC_phyco_epip = [GC_08_17_phyco_EPIP, GC_09_09_phyco_EPIP];
GC_phyco_epil = [GC_08_17_phyco_EPIL, GC_09_09_phyco_EPIL];

%GC_allChla_QA = [GC_0817_allChla(1:7), GC_0909_allChla(1:8),GC_0909_allChla(10:19)];
%GC_chla_filaEpip_QA = [GC_chla_filaEpip(1:7), GC_chla_filaEpip(8:17),GC_chla_filaEpip(18), GC_chla_filaEpip(20:25)];
%GC_allHoops_waterCorrected_QA = [GC_allHoops_waterCorrected_full(:,1:7), GC_allHoops_waterCorrected_full(:,8:17), GC_allHoops_waterCorrected_full(:,18),GC_allHoops_waterCorrected_full(:,20:25)];

% Remove sus points (more below!)
%GC_08_17_chlorophyll_content(1) = []; %1st point
%GC_0817_allChla(1) = [];
%GC_0909_allChla(9) = [];
%GC_0909_allChla(19) = [];
%GC_08_17_phycocyanin_content(1) = [];

%GC_09_09_chlorophyll_content_ritchie(19) = []; % 20th point
% GC_09_09_chlorophyll_content_hauer(9) = []; % 9th point
% GC_09_09_chlorophyll_content_hauer(19) = []; % 20th point
%GC_09_09_phycocyanin_content(9) = []; % 9th point
%GC_09_09_phycocyanin_content(19) = []; % 20th point
% Remove GC_0909 point 10 - seems suspect
%GC_allHoops_waterCorrected(:,15) = [];
%GC_allHoops(:,15) = [];
%allHoops(:,15) = [];
%GC_total_phycocyanin(:,15) = [];
%allHoops_waterCorrect(:,15) = [];

%BM_chlorophyll_content(3) = [];
%BM_chla_filaEpip(3) = [];
%BM_phycocyanin_content(3) = [];
%BM_allHoops_waterCorrected(:,3) = [];

% GC_09_09_chlorophyll_content_ritchie(19) = [];
% allHoops_waterCorrected_allData_noRmvl(:,33) = [];
% GC_total_chlorophyll_content_ritchie = [GC_08_17_chlorophyll_content, GC_09_09_chlorophyll_content_ritchie];
% GC_total_chlorophyll_content_ritchie_noZero = GC_total_chlorophyll_content_ritchie;
% GC_total_chlorophyll_content_ritchie_noZero(7) = [];
% GC_total_chlorophyll_content_ritchie_noZero(10) = [];

%Remove one more outlying point (to match above)
%GC_total_chlorophyll_content_ritchie(:,15) = [];
%GC_allChla(15)=[];
% 
% GC_allHoops_gradient = gradient(GC_allHoops_waterCorrected);
%GC_allHoops_sub150_gradient = gradient(GC_hoops_reflect_sub150);

% index = find(GC_total_chlorophyll_content_ritchie < 200);
% GC_total_chla_sub200 = GC_total_chlorophyll_content_ritchie(index);
% GC_allHoops_sub200 = GC_allHoops_waterCorrected(:, index);

% totalChla = [GC_total_chlorophyll_content_ritchie, BM_chlorophyll_content];
% totalChla_noQAQC = [GC_total_chla_ritchie_noRmvl, BM_chlorophyll_content];
% totalChla_noZero = [GC_total_chlorophyll_content_ritchie_noZero, BM_chlorophyll_content];
% 
% index = find(totalChla_allSites < 100);
% Total_chla_sub100 = totalChla_allSites(index);
% allHoops_sub100 = allHoops_waterCorrected_allData_noRmvl(:, index);
% 
% index = find(totalChla_allSites < 200 & totalChla_allSites > 100);
% Total_chla_100200 = totalChla_allSites(index);
% allHoops_100200 = allHoops_waterCorrected_allData_noRmvl(:, index);
% 
% index = find(totalChla_allSites > 200);
% Total_chla_plus200 = totalChla_allSites(index);
% allHoops_plus200 = allHoops_waterCorrected_allData_noRmvl(:, index);


%% Remove zero/suspect points for all-sites analysis

% allHoops_waterCorrected_allData_noRmvl(:,26) = [];
% allHoops_waterCorrected_allData_noRmvl(:,26) = [];
% totalChla_allSites(26) = [];
% totalChla_allSites(26) = [];
% 
% totalPhyco(26) = [];
% totalPhyco(26) = [];

%% Remove zero points for all-sites fila+epip analysis
% Chlorophyll:
% Chla_allSites_filaEpip(1) = [];
% Chla_allSites_filaEpip(25) = [];
% Chla_allSites_filaEpip(25) = [];
% allHoops_waterCorrected_allData_noRmvl(:,1) = [];
% allHoops_waterCorrected_allData_noRmvl(:,25) = [];
% allHoops_waterCorrected_allData_noRmvl(:,25) = [];

%Phyco:
% totalPhyco_EPIP(1) = [];
% allHoops_waterCorrected_allData_noRmvl(:,1) = [];
% totalPhyco_EPIP(1) = [];
% allHoops_waterCorrected_allData_noRmvl(:,1) = [];
% totalPhyco_EPIP(3) = [];
% allHoops_waterCorrected_allData_noRmvl(:,3) = [];
% totalPhyco_EPIP(15) = [];
% allHoops_waterCorrected_allData_noRmvl(:,15) = [];
% 
% totalPhyco_EPIP(21) = [];
% allHoops_waterCorrected_allData_noRmvl(:,21) = [];
% totalPhyco_EPIP(21) = [];
% allHoops_waterCorrected_allData_noRmvl(:,21) = [];

%% Remove zero points for all-sites epil analysis
% Chla_allSites_epil(32) = [];
% totalPhyco_EPIL(32) = [];
% allHoops_waterCorrected_allData_noRmvl(:,32) = [];

%% Remove zero points for BG 08-17 data
% BM_allHoops_waterCorrected(:,5) = [];
% BM_phyco_EPIL(5) = [];
% BM_chla_epil(5) = [];

%% Remove zero points for GC 08-17 data
%GC_0817_waterCorrect(:,1) = [];
%GC_0817_waterCorrect(:,1) = [];
%GC_0817_waterCorrect(:,3) = [];

%GC_08_17_chla_filaEpip(1) = [];

%GC_08_17_Phyco_EPIP(1) = [];
%GC_08_17_Phyco_EPIP(1) = [];
%GC_08_17_Phyco_EPIP(3) = [];

%% Remove zero/suspect points for GC 09-09 analysis
%GC_09_09_chlorophyll_content_ritchie(19) = []; % 19th point
%GC_09_09_chla_filaEpip(19) = []; % 19th point
%GC_09_09_chla_epil(19) = []; % 19th point
% GC_0909_waterCorrect(:,19) = [];
% GC_09_09_phycocyanin_content(19) = [];
% GC_09_09_phyco_EPIP(19) = [];
% GC_09_09_phyco_EPIL(19) = [];

%GC_09_09_phyco_EPIP(11) = [];
%GC_0909_waterCorrect(:,11) = [];


%% Remove points for GC all hoops analysis 
%total chla
% GC_chla_total(26) = [];
% GC_chla_total(end) = [];
% GC_allHoops_waterCorrected_full(:,26) = [];
% GC_allHoops_waterCorrected_full(:,end) = [];
% % % total phyco
% GC_total_phycocyanin(26) = [];
% GC_total_phycocyanin(end) = [];
% 
% GC_chla_filaEpip(1) = [];
% GC_chla_filaEpip(25) = [];
% GC_chla_filaEpip(end) = [];
% GC_allHoops_waterCorrected_full(:,1) = [];
% GC_allHoops_waterCorrected_full(:,25) = [];
% GC_allHoops_waterCorrected_full(:,end) = [];


%phyco
% GC_allHoops_waterCorrected_full(:,1) = [];
% GC_allHoops_waterCorrected_full(:,1) = [];
% GC_allHoops_waterCorrected_full(:,3) = [];
% GC_allHoops_waterCorrected_full(:,15) = [];
% GC_allHoops_waterCorrected_full(:,23) = [];
% GC_allHoops_waterCorrected_full(:,end-1) = [];
% GC_allHoops_waterCorrected_full(:,end) = [];
% GC_phyco_epip(1) = [];
% GC_phyco_epip(1) = [];
% GC_phyco_epip(3) = [];
% GC_phyco_epip(15) = [];
% GC_phyco_epip(23) = [];
% GC_phyco_epip(end-1) = [];
% GC_phyco_epip(end) = [];

% GC_chla_epil(26) = [];
% GC_chla_epil(end) = [];
% GC_phyco_epil(26) = [];
% GC_phyco_epil(end) = [];
% GC_allHoops_waterCorrected_full(:,26) = [];
% GC_allHoops_waterCorrected_full(:,end) = [];

%% Remove points for gunk index prediction
% NaN points:
%  gunkIndex(1) = [];
%  gunkIndex(14) = [];
%  gunkIndex(18) = [];
%  allHoops_waterCorrected_allData_noRmvl(:,1) = [];
%  allHoops_waterCorrected_allData_noRmvl(:,14) = [];
%  allHoops_waterCorrected_allData_noRmvl(:,20) = [];
% % Zero points:
%  gunkIndex(1) = [];
%  gunkIndex(3) = [];
%  gunkIndex(20) = [];
%  allHoops_waterCorrected_allData_noRmvl(:,1) = [];
%  allHoops_waterCorrected_allData_noRmvl(:,3) = [];
%  allHoops_waterCorrected_allData_noRmvl(:,20) = [];

%gunkIndex_edit = gunkIndex(gunkIndex>0 & gunkIndex<999);
%allHoops_waterCorrected_allData_noRmvl = allHoops_waterCorrected_allData_noRmvl(:,gunkIndex>0 & gunkIndex<999);

% Smooth data
order = 3;
framelen = 5;
%GC_allHoops_smooth = sgolayfilt(GC_allHoops,order,framelen);
%GC_0817_waterCorrect = sgolayfilt(GC_0817_waterCorrect, order, framelen);
%GC_0909_waterCorrect = sgolayfilt(GC_0909_waterCorrect, order, framelen);
%BM_allHoops_waterCorrected = sgolayfilt(BM_allHoops_waterCorrected, order, framelen);
%BM_allHoops_waterCorrected_smooth1 = sgolayfilt(BM_allHoops_waterCorrected,order,framelen);
%BM_allHoops_waterCorrected_smooth2 = sgolayfilt(BM_allHoops_waterCorrected_smooth1,order,framelen);

% Build ratios using GC data with outliers removed
% for m = 1:size(wavelengths,1)
%     for n = 1:size(wavelengths,1)
%         %ratios_GC(n).num(m).den = GC_allHoops_waterCorrected_full(n,:) ./ GC_allHoops_waterCorrected_full(m,:);
%         ratios_GC(n).num(m).den = GC_allHoops_waterCorrected_QA(n,:) ./ GC_allHoops_waterCorrected_QA(m,:);        
%         %ratios_GC(n).num(m).den = GC_allHoops_full(n,:) ./ GC_allHoops_full(m,:);
%         %ratios(n).num(m).den = GC_allHoops_QAQC_noZero(n,:) ./ GC_allHoops_QAQC_noZero(m,:);
%     end
%     fprintf('Building GC ratios %.0f of %.0f \n', m, size(wavelengths,1))
% end

%Build ratios using 08 17 GC data
for m = 1:size(wavelengths,1)
   for n = 1:size(wavelengths,1)
       ratios_GC0817(n).num(m).den = GC_0817_waterCorrect(n,:) ./ GC_0817_waterCorrect(m,:);
   end
   fprintf('Building GC 08-17 ratios %.0f of %.0f \n', m, size(wavelengths,1))
end

%Build ratios using 09-09 GC data
for m = 1:size(wavelengths,1)
   for n = 1:size(wavelengths,1)
       ratios_GC0909(n).num(m).den = GC_0909_waterCorrect(n,:)./ GC_0909_waterCorrect(m,:);
   end
   fprintf('Building GC 09-09 ratios %.0f of %.0f \n', m, size(wavelengths,1))
end

%Build ratios using complete BM data
for m = 1:size(wavelengths,1)
   for n = 1:size(wavelengths,1)
       ratios_BM(n).num(m).den = BM_allHoops_waterCorrected(n,:) ./ BM_allHoops_waterCorrected(m,:);
   end
   fprintf('Building BM ratios %.0f of %.0f \n', m, size(wavelengths,1))
end

% Build ratios using complete data (GC + BM) with removals
if allHoops_on == 1
    for m = 1:size(wavelengths,1)
        for n = 1:size(wavelengths,1)
            GC_allHoops_Ratio(n).num(m).den = GC_allHoops_waterCorrected_full(n,:)./GC_allHoops_waterCorrected_full(m,:);
        end
        fprintf('Building all hoops (GC + BM) ratio %.0f of %.0f \n', m, size(wavelengths,1))
    end
else
end

%Build ratios using complete data (GC + BM) with no removals (for phyco analysis)
if allHoops_on == 1
    for m = 1:size(wavelengths,1)
        for n = 1:size(wavelengths,1)
            allHoops_ALL(n).num(m).den = allHoops_allData_noRmvl(n,:)./allHoops_allData_noRmvl(m,:);
            allHoops_waterCorrected_ALL(n).num(m).den = allHoops_waterCorrected_allData_noRmvl(n,:)./allHoops_waterCorrected_allData_noRmvl(m,:);
        end
        fprintf('Building all hoops (GC + BM, no removal) ratio %.0f of %.0f \n', m, size(wavelengths,1))
    end
else
end

%Build ratios for gradient fit
%gradientAllHoops_smooth = gradient(allHoops_smooth);
if gradientOn == 1
    for m = 1:size(wavelengths,1)
        for n = 1:size(wavelengths,1)
            gradientAllHoops(n).num(m).den = GC_allHoops_gradient(n,:)./GC_allHoops_gradient(m,:);
        end
        fprintf('Building gradient %.0f of %.0f \n', m, size(wavelengths,1))
    end
else
end

% Sub-100 chl-a pairs, already depth corrected
%allHoopsSub100 = nchoosek(GC_allHoops_sub100, 2);
if sub100_on == 1
    for m = 1:size(wavelengths,1)
        for n = 1:size(wavelengths,1)
            AllHoopsSub100(n).num(m).den = allHoops_sub100(n,:)./allHoops_sub100(m,:);
        end
        fprintf('Building sub-100 %.0f of %.0f \n', m, size(wavelengths,1))
    end
else
end

% Sub-150 chl-a pairs, already depth corrected
%allHoopsSub150 = nchoosek(GC_allHoops_sub150, 2);
if tween100200_on == 1
    for m = 1:size(wavelengths,1)
        for n = 1:size(wavelengths,1)
            AllHoops_100200(n).num(m).den = allHoops_100200(n,:)./allHoops_100200(m,:);
        end
        fprintf('Building 100-200 %.0f of %.0f \n', m, size(wavelengths,1))
    end
else
end

% Sub 200 chl-a separation, already depth corrected
%allHoopsSub200 = nchoosek(GC_allHoops_sub200, 2);
% if sub200_on == 1
%     for m = 1:size(wavelengths,1)
%         for n = 1:size(wavelengths,1)
%             AllHoopsSub200(n).num(m).den = allHoops_sub200(n,:)./allHoops_sub200(m,:);
%         end
%         fprintf('Building sub-200 %.0f of %.0f \n', m, size(wavelengths,1))
%     end
% else
% end

% Plus 200 chl-a separation, already depth corrected
%allHoopsPlus200 = nchoosek(GC_allHoops_Plus200, 2);
if plus200_on == 1
    for m = 1:size(wavelengths,1)
        for n = 1:size(wavelengths,1)
            AllHoopsPlus200(n).num(m).den = allHoops_plus200(n,:)./allHoops_plus200(m,:);
        end
        fprintf('Building plus-200 %.0f of %.0f \n', m, size(wavelengths,1))
    end
else
end

% Three band combination
if threeBand_on == 1
    for m = 1:size(wavelengths,1)
        for n = 1:size(wavelengths,1)
            for p = 1:size(wavelengths,1)
            AllHoops_threeBand(n).num(m).den(p).multiplier = (1./allHoops_waterCorrect(n,:) - 1./allHoops_waterCorrect(m,:) .* 1./allHoops_waterCorrect(p,:));
            end
        end
        fprintf('Building three band %.0f of %.0f \n', m, size(wavelengths,1))
    end
else
end

%% Select the desired fit with the following options:
% 1: Linear fit
% 2: Quadratic fit
% 3: Exponential model
% 4: Power model

fit = 1;

Rsquare_linear = 0;
Rsquare_quad = 0;
Rsquare_exp = 0;
Rsquare_power = 0;
errorLinear = 100000;
RsquareLinearFull = array2table(zeros(size(nchoosek(wavelengths,2),1),3), "VariableNames",{'R2', 'Wavelength1', 'Wavelength2'});
Rsquare_test = 0;
r = 1;
w1 = 1;
w2 = 1;
for n = 1:size(wavelengths,1)
    ratios_GC(n).LM(m).wavelength = wavelengths(n);
    for m = 1:size(wavelengths,1)
        if fit == 1
            ratios_GC(n).LM(m).wavelength = wavelengths(m);
            %[~,~,~,~,stats] = regress(BM_chlorophyll_content', [ones(size(ratios_BM(n).num(m).den')) ratios_BM(n).num(m).den']);
            %[~,~,~,~,stats] = regress(BM_chla_filaEpip', [ones(size(ratios_BM(n).num(m).den')) ratios_BM(n).num(m).den']);
            %[~,~,~,~,stats] = regress(BM_chla_epil', [ones(size(ratios_BM(n).num(m).den')) ratios_BM(n).num(m).den']);
            %[~,~,~,~,stats] = regress(GC_08_17_chlorophyll_content', [ones(size(ratios_GC0817(n).num(m).den')) ratios_GC0817(n).num(m).den']);
            %[~,~,~,~,stats] = regress(GC_08_17_chla_filaEpip', [ones(size(ratios_GC0817(n).num(m).den')) ratios_GC0817(n).num(m).den']);
            %[~,~,~,~,stats] = regress(GC_08_17_chla_epil', [ones(size(ratios_GC0817(n).num(m).den')) ratios_GC0817(n).num(m).den']);
            %[~,~,~,~,stats] = regress(GC_09_09_chlorophyll_content_ritchie', [ones(size(ratios_GC0909(n).num(m).den')) ratios_GC0909(n).num(m).den']);
            %[~,~,~,~,stats] = regress(GC_09_09_chla_filaEpip', [ones(size(ratios_GC0909(n).num(m).den')) ratios_GC0909(n).num(m).den']);
            %[~,~,~,~,stats] = regress(GC_09_09_chla_epil', [ones(size(ratios_GC0909(n).num(m).den')) ratios_GC0909(n).num(m).den']);
            %[~,~,~,~,stats] = regress(GC_total_chlorophyll_content_ritchie', [ones(size(ratios_GC(n).num(m).den')) ratios_GC(n).num(m).den']);
            %[~,~,~,~,stats] = regress(GC_chla_filaEpip', [ones(size(ratios_GC(n).num(m).den')) ratios_GC(n).num(m).den']);
            %[~,~,~,~,stats] = regress(GC_chla_filaEpip_QA', [ones(size(ratios_GC(n).num(m).den')) ratios_GC(n).num(m).den']);
            %[~,~,~,~,stats] = regress(totalChla_noQAQC', [ones(size(allHoops_waterCorrected_ALL(n).num(m).den')) allHoops_waterCorrected_ALL(n).num(m).den']);
            %[~,~,~,~,stats] = regress(totalChla_noQAQC', [ones(size(allHoops_ALL(n).num(m).den')) allHoops_ALL(n).num(m).den']);
            %[~,~,~,~,stats] = regress(totalChla_allSites', [ones(size(allHoops_ALL(n).num(m).den')) allHoops_ALL(n).num(m).den']);
            %[~,~,~,~,stats] = regress(totalChla_allSites', [ones(size(allHoops_waterCorrected_ALL(n).num(m).den')) allHoops_waterCorrected_ALL(n).num(m).den']);
            %[~,~,~,~,stats] = regress(Chla_allSites_filaEpip', [ones(size(allHoops_waterCorrected_ALL(n).num(m).den')) allHoops_waterCorrected_ALL(n).num(m).den']);
            %[~,~,~,~,stats] = regress(Chla_allSites_epil', [ones(size(allHoops_waterCorrected_ALL(n).num(m).den')) allHoops_waterCorrected_ALL(n).num(m).den']);
            %[~,~,~,~,stats] = regress(gunkIndex_edit', [ones(size(allHoops_waterCorrected_ALL(n).num(m).den')) allHoops_waterCorrected_ALL(n).num(m).den']);
            %[~,~,~,~,stats] = regress(GC_chla_total', [ones(size(GC_allHoops_Ratio(n).num(m).den')) GC_allHoops_Ratio(n).num(m).den']);
            %[~,~,~,~,stats] = regress(GC_chla_filaEpip', [ones(size(GC_allHoops_Ratio(n).num(m).den')) GC_allHoops_Ratio(n).num(m).den']);
            [~,~,~,~,stats] = regress(GC_chla_epil', [ones(size(GC_allHoops_Ratio(n).num(m).den')) GC_allHoops_Ratio(n).num(m).den']);

            %linearRatio = allHoops_waterCorrected_allData_noRmvl(find(wavelengths==684.16),:) ./ allHoops_waterCorrected_allData_noRmvl(find(wavelengths==673.55),:);
            %[~,~,~,~,stats] = regress(totalChla_allSites', [ones(size(linearRatio')) linearRatio']);
            
            Rsquare_test = stats(1);
            RsquareLinearFull(r, 'R2') = {stats(1)};
            r = r+1;
            RsquareLinearFull(w1, 'Wavelength1') = {wavelengths(n)};
            RsquareLinearFull(w2, 'Wavelength2') = {wavelengths(m)};
            w1 = w1+1;
            w2 = w2+1;
            %errorTest = stats(end);
            if Rsquare_test > Rsquare_linear
                Rsquare_linear = Rsquare_test;
                RsquareLinear_wave1 = wavelengths(n);
                RsquareLinear_wave2 = wavelengths(m);
            else
            end

        elseif fit == 2
            ratios_GC(n).LM(m).wavelength = wavelengths(m);
            [~,~,~,~,stats] = regress(gunkIndex_edit', [ones(size(allHoops_waterCorrected_ALL(n).num(m).den')) allHoops_waterCorrected_ALL(n).num(m).den' (allHoops_waterCorrected_ALL(n).num(m).den').^2]);
            Rsquare_test = stats(1);
            if Rsquare_test > Rsquare_quad
                Rsquare_quad = Rsquare_test;
                RsquareQuad_wave1 = wavelengths(n);
                RsquareQuad_wave2 = wavelengths(m);
            end

        elseif fit == 3
            ratios_GC(n).LM(m).wavelength = wavelengths(m);
            %GC_total_chlorophyll_content_ritchie(GC_total_chlorophyll_content_ritchie == 0) = 1;
            natLog = log(gunkIndex_edit);
            [~,~,~,~,stats] = regress(natLog', [ones(size(allHoops_waterCorrected_ALL(n).num(m).den')) allHoops_waterCorrected_ALL(n).num(m).den']);
            Rsquare_test = stats(1);
            if Rsquare_test > Rsquare_exp
                Rsquare_exp = Rsquare_test;
                RsquareExp_wave1 = wavelengths(n);
                RsquareExp_wave2 = wavelengths(m);
            end
        elseif fit == 4
            ratios_GC(n).LM(m).wavelength = wavelengths(m);
            %GC_total_chlorophyll_content_ritchie(GC_total_chlorophyll_content_ritchie == 0) = 1;
            natLog = log(gunkIndex_edit);
            natLogRatio = log(allHoops_waterCorrected_ALL(n).num(m).den);
            [~,~,~,~,stats] = regress(natLog', [ones(size(natLogRatio')) natLogRatio']);
            Rsquare_test = stats(1);
            if Rsquare_test > Rsquare_power
                Rsquare_power = Rsquare_test;
                RsquarePower_wave1 = wavelengths(n);
                RsquarePower_wave2 = wavelengths(m);
            end
        end
    end
    fprintf('Building chlorophyll fit %.0f of %.0f \n', n, size(wavelengths,1))
end

%% Plots and display for above
if fit == 1
    %linearRatio = BM_allHoops_waterCorrected(find(wavelengths==RsquareLinear_wave1),:)./BM_allHoops_waterCorrected(find(wavelengths==RsquareLinear_wave2),:);
    %linearRatio = GC_0817_waterCorrect(find(wavelengths==RsquareLinear_wave1),:) ./ GC_0817_waterCorrect(find(wavelengths==RsquareLinear_wave2),:);
    %linearRatio = GC_0909_waterCorrect(find(wavelengths==RsquareLinear_wave1),:) ./ GC_0909_waterCorrect(find(wavelengths==RsquareLinear_wave2),:);
    linearRatio = GC_allHoops_waterCorrected_full(find(wavelengths==RsquareLinear_wave1),:)./GC_allHoops_waterCorrected_full(find(wavelengths==RsquareLinear_wave2),:);
    %linearRatio = allHoops_waterCorrected_allData_noRmvl(find(wavelengths==RsquareLinear_wave1),:) ./ allHoops_waterCorrected_allData_noRmvl(find(wavelengths==RsquareLinear_wave2),:);
    
    %linearRatio = allHoops_waterCorrected_allData_noRmvl(find(wavelengths==684.16),:) ./ allHoops_waterCorrected_allData_noRmvl(find(wavelengths==673.55),:);
    
    %linearRatio = allHoops_allData_noRmvl(find(wavelengths==RsquareLinear_wave1),:)./ allHoops_allData_noRmvl(find(wavelengths==RsquareLinear_wave2),:);

    %b = regress(BM_chlorophyll_content', [ones(size(linearRatio')) linearRatio']);
    %b = regress(BM_chla_filaEpip', [ones(size(linearRatio')) linearRatio']);
    %b = regress(BM_chla_epil', [ones(size(linearRatio')) linearRatio']);
    %b = regress(GC_08_17_chlorophyll_content', [ones(size(linearRatio')) linearRatio']);
    %b = regress(GC_08_17_chla_filaEpip', [ones(size(linearRatio')) linearRatio']);
    %b = regress(GC_08_17_chla_epil', [ones(size(linearRatio')) linearRatio']);
    %b = regress(GC_chla_total', [ones(size(linearRatio')) linearRatio']);
    %b = regress(GC_chla_filaEpip', [ones(size(linearRatio')) linearRatio']);
    b = regress(GC_chla_epil', [ones(size(linearRatio')) linearRatio']);
    %b = regress(GC_09_09_chlorophyll_content_ritchie', [ones(size(linearRatio')) linearRatio']);
    %b = regress(GC_09_09_chla_filaEpip', [ones(size(linearRatio')) linearRatio']);
    %b = regress(GC_09_09_chla_epil', [ones(size(linearRatio')) linearRatio']);
    %b = regress(totalChla_noQAQC', [ones(size(linearRatio')) linearRatio']);
    %b = regress(totalChla_allSites', [ones(size(linearRatio')) linearRatio']);
    %b = regress(Chla_allSites_filaEpip', [ones(size(linearRatio')) linearRatio']);
    %b = regress(Chla_allSites_epil', [ones(size(linearRatio')) linearRatio']);
    %b = regress(gunkIndex_edit', [ones(size(linearRatio')) linearRatio']);

    disp('  ')
    disp('Linear Fit')
    disp(['     Maximum R-squared: ' num2str(Rsquare_linear)])
    disp(['     Optimal band ratio: (' num2str(RsquareLinear_wave1) '/' num2str(RsquareLinear_wave2) ')'])
    disp(['     Optimal band ratio: (' num2str(684) '/' num2str(674) ')'])
    disp(['     X vs. d relation: d = ' num2str(b(1),4) ' + ' ...
        num2str(b(2),4) 'X'])

    chlaPrediction = b(1)+b(2) .* linearRatio;
    %rmse = sqrt(mean((BM_chlorophyll_content - chlaPrediction).^2))
    %rmse = sqrt(mean((BM_chla_filaEpip - chlaPrediction).^2))
    %rmse = sqrt(mean((BM_chla_epil - chlaPrediction).^2))
    %rmse = sqrt(mean((GC_08_17_chlorophyll_content - chlaPrediction).^2))
    %rmse = sqrt(mean((GC_08_17_chla_filaEpip - chlaPrediction).^2))
    %rmse = sqrt(mean((GC_08_17_chla_epil - chlaPrediction).^2))
    %rmse = sqrt(mean((GC_09_09_chlorophyll_content_ritchie - chlaPrediction).^2))
    %rmse = sqrt(mean((GC_09_09_chla_filaEpip - chlaPrediction).^2))
    %rmse = sqrt(mean((GC_09_09_chla_epil - chlaPrediction).^2))
    %rmse = sqrt(mean((GC_chla_total - chlaPrediction).^2))
    %rmse = sqrt(mean((GC_chla_filaEpip - chlaPrediction).^2))
    rmse = sqrt(mean((GC_chla_epil - chlaPrediction).^2))
    %rmse = sqrt(mean((totalChla_noQAQC - chlaPrediction).^2))
    %rmse = sqrt(mean((totalChla_allSites - chlaPrediction).^2))
    %rmse = sqrt(mean((Chla_allSites_filaEpip - chlaPrediction).^2))
    %rmse = sqrt(mean((Chla_allSites_epil - chlaPrediction).^2))
    %rmse = sqrt(mean((gunkIndex_edit - chlaPrediction).^2))

    % Plot
    figure('position',[50 50 1600 1000])
    %tiledlayout(1,2)
    %nexttile
    %plot(linearRatio, BM_chlorophyll_content,'o', 'linewidth', 2);
    %plot(linearRatio, BM_chla_filaEpip,'o', 'linewidth', 2);
    %plot(linearRatio, BM_chla_epil,'o', 'linewidth', 2);
    %plot(linearRatio, GC_08_17_chlorophyll_content,'o', 'linewidth', 2);
    %plot(linearRatio, GC_08_17_chla_filaEpip,'o', 'linewidth', 2);
    %plot(linearRatio, GC_08_17_chla_epil,'o', 'linewidth', 2);
    %plot(linearRatio(1:7), GC_chla_total(1:7),'s', linearRatio(8:end), GC_chla_total(8:end),'d', 'linewidth', 2);
    %plot(linearRatio(1:6), GC_chla_filaEpip(1:6),'o', linearRatio(7:end), GC_chla_filaEpip(7:end),'o', 'linewidth', 2);
    plot(linearRatio(1:7), GC_chla_epil(1:7),'o', linearRatio(8:end), GC_chla_epil(8:end),'o', 'linewidth', 2);
    %plot(linearRatio, GC_09_09_chla_filaEpip,'o', 'linewidth', 2);
    %plot(linearRatio, GC_09_09_chla_epil,'o', 'linewidth', 2);
    %plot(linearRatio, GC_total_chlorophyll_content_ritchie,'o', 'linewidth', 2);
    %plot(linearRatio, GC_chla_filaEpip,'o', 'linewidth', 2);
    %plot(linearRatio, GC_chla_filaEpip_QA,'o', 'linewidth', 2);
    %plot(linearRatio, totalChla_noQAQC,'o', 'linewidth', 2);
    %plot(linearRatio(1:7), totalChla_allSites(1:7),'o', linearRatio(8:25), totalChla_allSites(8:25),'s', linearRatio(26:end), totalChla_allSites(26:end),'d', 'linewidth', 2); legend('08-17 Gold Creek', '09-09 Gold Creek', '08-17 Bear Gulch');
    %plot(linearRatio(1:6), Chla_allSites_filaEpip(1:6),'o', linearRatio(7:24), Chla_allSites_filaEpip(7:24),'s', linearRatio(25:end), Chla_allSites_filaEpip(25:end),'d', 'linewidth', 2); legend('08-17 Gold Creek', '09-09 Gold Creek', '08-17 Bear Gulch');
    %plot(linearRatio(1:7), Chla_allSites_epil(1:7),'o', linearRatio(8:14), Chla_allSites_epil(8:14),'s', linearRatio(15:end), Chla_allSites_epil(15:end),'d', 'linewidth', 2); legend('08-17 Gold Creek', '08-17 Bear Gulch', '09-09 Gold Creek');
    %plot(linearRatio, gunkIndex_edit, 'o');
    % plot(linearRatio(1:7), gunkIndex_edit(1:7),'o', linearRatio(8:14), gunkIndex_edit(8:14),'s', linearRatio(15:end), gunkIndex_edit(15:end),'d', 'linewidth', 2); legend('08-17 Gold Creek', '08-17 Bear Gulch', '09-09 Gold Creek');
    
    axLim = axis;
    hold on
    plot(axLim(1:2),b(1)+b(2)*axLim(1:2),'k-','linewidth',3);
    axis(axLim);
    text(0.1,0.9,char(['d = ' num2str(b(1),4) ' + ' num2str(b(2),4) 'X '],...
        ['R^2 = ' num2str(Rsquare_linear,4) '; RMSE = ' num2str(rmse,4) ' mg/m^2']),...
        'units','normalized','fontname','arial','fontsize',12)
    xlabel(['Band Ratio (' num2str(RsquareLinear_wave1,3) '/' num2str(RsquareLinear_wave2,3) ')'])
    %xlabel(['Band Ratio (' num2str(684,3) '/' num2str(674,3) ')'])
    ylabel('Chl-a Concentration [mg/m^2]')
    title('Chl-a Concentation: Linear Model')
    hold off

elseif fit == 2
    quadRatio = allHoops_waterCorrected_allData_noRmvl(find(wavelengths==RsquareQuad_wave1),:) ./ allHoops_waterCorrected_allData_noRmvl(find(wavelengths==RsquareQuad_wave1),:);
    b = regress(GC_09_09_chlorophyll_content_ritchie', [ones(size(quadRatio')) quadRatio' quadRatio'.^2]);
    %b = regress(gunkIndex_edit', [ones(size(quadRatio')) quadRatio' quadRatio'.^2]);

    disp('  ')
    disp('Quadratic Fit')
    disp(['     Maximum R-squared: ' num2str(Rsquare_quad)])
    disp(['     Optimal band ratio: (' num2str(RsquareQuad_wave1) '/' num2str(RsquareQuad_wave2) ')'])
    disp(['     X vs. d relation: d = ' num2str(b(1),4) ' + ' ...
        num2str(b(2),4) 'X + ' num2str(b(3),4) 'X^2'])


    chlaPrediction = b(1) + b(2) .* (quadRatio + b(3)) .* quadRatio.^2;
    rmse = sqrt(mean((GC_09_09_chlorophyll_content_ritchie - chlaPrediction).^2));
    %rmse = sqrt(mean((gunkIndex_edit - chlaPrediction).^2))

    % Plot
    figure('position',[50 50 1600 800])
    %tiledlayout(1,2)
    %nexttile
    plot(quadRatio, GC_09_09_chlorophyll_content_ritchie,'b.');
    %plot(quadRatio, gunkIndex_edit,'b.');
    axLim = axis;
    hold on
    plot(linspace(axLim(1),axLim(2),200),...
        b(1) + b(2).*linspace(axLim(1),axLim(2),200) + ...
        b(3).*linspace(axLim(1),axLim(2),200).^2,'k-','linewidth',2)
    axis(axLim);
    text(0.1,0.9,char(['d = ' num2str(b(1),4) ' + ' num2str(b(2),4) 'X + ' num2str(b(3),4) 'X^2'],...
        ['R^2 = ' num2str(Rsquare_quad,4) '; RMSE = ' num2str(rmse,4) ' mg/m^2']),...
        'units','normalized','fontname','arial','fontsize',12)
    xlabel(['X = (' num2str(RsquareQuad_wave1,3) '/' num2str(RsquareQuad_wave2,3) ')'])
    ylabel('Chl-a Concentration [mg/m^2]')
    title('Quadratic Model')

elseif fit == 3
    expRatio = allHoops_waterCorrected_allData_noRmvl(find(wavelengths==RsquareExp_wave1),:) ./ allHoops_waterCorrected_allData_noRmvl(find(wavelengths==RsquareExp_wave2),:);
    %GC_total_chlorophyll_content_ritchie(GC_total_chlorophyll_content_ritchie == 0) = 1;

    b = regress(log(gunkIndex_edit'), [ones(size(expRatio')) expRatio']);
    a = exp(b(1));
    c = b(2);

    disp('  ')
    disp('Exponential Fit')
    disp(['     Maximum R-squared: ' num2str(Rsquare_exp)])
    disp(['     Optimal band ratio: (' num2str(RsquareExp_wave1) '/' num2str(RsquareExp_wave2) ')'])
    disp(['     X vs. d relation: d = ' num2str(a,4) 'e^{' ...
        num2str(c,4) 'X}'])

    chlaPrediction = a*exp(c*expRatio);
    rmse = sqrt(mean((gunkIndex_edit - chlaPrediction).^2));

    figure('position',[100 100 1600 800])
    %tiledlayout(1,2)
    %nexttile
    plot(expRatio,gunkIndex_edit,'b.');
    axLim = axis;
    hold on
    plot(linspace(axLim(1),axLim(2),200),...
        a*exp(c*linspace(axLim(1),axLim(2),200)),'k-','linewidth',2)
    axis(axLim);
    text(0.1,0.9,char(['d = ' num2str(a,4) 'e^{' ...
        num2str(c,4) 'X}'],['R^2 = ' num2str(Rsquare_exp,4) ...
        '; RMSE = ' num2str(rmse,4) ' mg/m^2']),...
        'units','normalized','fontname','arial','fontsize',12)
    xlabel(['X = log(R' num2str(RsquareExp_wave1,3) '/R' num2str(RsquareExp_wave2,3) ')'])
    ylabel('Depth (m)')
    title('Exponential Model')

elseif fit == 4
    powerRatio = allHoops_waterCorrected_allData_noRmvl(find(wavelengths==RsquarePower_wave1),:) ./ allHoops_waterCorrected_allData_noRmvl(find(wavelengths==RsquarePower_wave2),:);
    %GC_total_chlorophyll_content_ritchie(GC_total_chlorophyll_content_ritchie == 0) = 1;
    b = regress(log(gunkIndex_edit'), [ones(size(powerRatio')) log(powerRatio')]);
    a = exp(b(1));
    c = b(2);

    disp('  ')
    disp('Power Model Fit')
    disp(['     Maximum R-squared: ' num2str(Rsquare_power)])
    disp(['     Optimal band ratio: (' num2str(RsquarePower_wave1) '/' num2str(RsquarePower_wave2) ')'])
    disp(['     X vs. d relation: d = ' num2str(a,4) 'X^{' ...
        num2str(c,4) '}'])

    chlaPrediction = a*powerRatio.^c;
    rmse = sqrt(mean((gunkIndex_edit-chlaPrediction).^2));

    figure('position',[100 100 1600 800])
    tiledlayout(1,2)
    nexttile
    plot(powerRatio,gunkIndex_edit,'b.');
    axLim = axis;
    hold on
    plot(linspace(axLim(1),axLim(2),200),...
        a*linspace(axLim(1),axLim(2),200).^c,'k-','linewidth',2)
    axis(axLim);
    text(0.1,0.9,char(['d = ' num2str(a,4) 'e^{' ...
        num2str(c,4) 'X}'],['R^2 = ' num2str(Rsquare_power,4) ...
        '; RMSE = ' num2str(rmse,4) ' mg/m^2']),...
        'units','normalized','fontname','arial','fontsize',12)
    xlabel(['X = log(R' num2str(RsquarePower_wave1,3) '/R' num2str(RsquarePower_wave2,3) ')'])
    ylabel('Chl-a Concentration (mg/m^2)')
    title('Power Model')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Phycocyanin version of above
Rsquare_PC = 0;
Rsquare_PCTest = 0;
RsquarePCFull = array2table(zeros(size(nchoosek(wavelengths,2),1),3), "VariableNames",{'R2', 'Wavelength1', 'Wavelength2'});
r = 1;
w1 = 1;
w2 = 1;
for n = 1:size(wavelengths,1)
    ratios_GC(n).LM(m).wavelength = wavelengths(n);
    for m = 1:size(wavelengths,1)
        ratios_GC(n).LM(m).wavelength = wavelengths(m);
       %[~,~,~,~,stats] = regress(BM_phycocyanin_content', [ones(size(ratios_BM(n).num(m).den')) ratios_BM(n).num(m).den']);
       %[~,~,~,~,stats] = regress(BM_phyco_EPIP', [ones(size(ratios_BM(n).num(m).den')) ratios_BM(n).num(m).den']);
       %[~,~,~,~,stats] = regress(BM_phyco_EPIL', [ones(size(ratios_BM(n).num(m).den')) ratios_BM(n).num(m).den']);
       %[~,~,~,~,stats] = regress(GC_08_17_phycocyanin_content', [ones(size(ratios_GC0817(n).num(m).den')) ratios_GC0817(n).num(m).den']);
       %[~,~,~,~,stats] = regress(GC_08_17_Phyco_EPIP', [ones(size(ratios_GC0817(n).num(m).den')) ratios_GC0817(n).num(m).den']);
       %[~,~,~,~,stats] = regress(GC_08_17_phyco_EPIL', [ones(size(ratios_GC0817(n).num(m).den')) ratios_GC0817(n).num(m).den']);
       %[~,~,~,~,stats] = regress(GC_09_09_phycocyanin_content', [ones(size(ratios_GC0909(n).num(m).den')) ratios_GC0909(n).num(m).den']);
       %[~,~,~,~,stats] = regress(GC_09_09_phyco_EPIP', [ones(size(ratios_GC0909(n).num(m).den')) ratios_GC0909(n).num(m).den']);
       %[~,~,~,~,stats] = regress(GC_09_09_phyco_EPIL', [ones(size(ratios_GC0909(n).num(m).den')) ratios_GC0909(n).num(m).den']);
       %[~,~,~,~,stats] = regress(GC_total_phycocyanin', [ones(size(GC_allHoops_Ratio(n).num(m).den')) GC_allHoops_Ratio(n).num(m).den']);
       %[~,~,~,~,stats] = regress(GC_phyco_epip', [ones(size(GC_allHoops_Ratio(n).num(m).den')) GC_allHoops_Ratio(n).num(m).den']);
       [~,~,~,~,stats] = regress(GC_phyco_epil', [ones(size(GC_allHoops_Ratio(n).num(m).den')) GC_allHoops_Ratio(n).num(m).den']);
       %[~,~,~,~,stats] = regress(GC_allPhyco_QA', [ones(size(ratios_GC(n).num(m).den')) ratios_GC(n).num(m).den']);
       %[~,~,~,~,stats] = regress(totalPhyco', [ones(size(allHoops_waterCorrected_ALL(n).num(m).den')) allHoops_waterCorrected_ALL(n).num(m).den']);
       %[~,~,~,~,stats] = regress(totalPhyco', [ones(size(allHoops_ALL(n).num(m).den')) allHoops_ALL(n).num(m).den']);
       %[~,~,~,~,stats] = regress(totalPhyco_EPIP', [ones(size(allHoops_waterCorrected_ALL(n).num(m).den')) allHoops_waterCorrected_ALL(n).num(m).den']);
       %[~,~,~,~,stats] = regress(totalPhyco_EPIL', [ones(size(allHoops_waterCorrected_ALL(n).num(m).den')) allHoops_waterCorrected_ALL(n).num(m).den']);
       Rsquare_PCTest = stats(1);
        RsquarePCFull(r, 'R2') = {stats(1)};
        r = r+1;
        RsquarePCFull(w1, 'Wavelength1') = {wavelengths(n)};
        RsquarePCFull(w1, 'Wavelength2') = {wavelengths(m)};
        w1 = w1+1;
        w2 = w2+1;
        if Rsquare_PCTest > Rsquare_PC
            Rsquare_PC = Rsquare_PCTest; 
            RsquarePC_wave1 = wavelengths(n);
            RsquarePC_wave2 = wavelengths(m);
        else
        end
    end
    fprintf('Building phycocyanin fit %.0f of %.0f \n', n, size(wavelengths,1))
end

%% Plots for Phycocyanin

%PC_Ratio = BM_allHoops_waterCorrected(find(wavelengths==RsquarePC_wave1),:) ./ BM_allHoops_waterCorrected(find(wavelengths==RsquarePC_wave2),:);
%PC_Ratio = GC_0817_waterCorrect(find(wavelengths==RsquarePC_wave1),:) ./ GC_0817_waterCorrect(find(wavelengths==RsquarePC_wave2),:);
%PC_Ratio = GC_0909_waterCorrect(find(wavelengths==RsquarePC_wave1),:) ./ GC_0909_waterCorrect(find(wavelengths==RsquarePC_wave2),:);
PC_Ratio = GC_allHoops_waterCorrected_full(find(wavelengths==RsquarePC_wave1),:) ./ GC_allHoops_waterCorrected_full(find(wavelengths==RsquarePC_wave2),:);
%PC_Ratio = GC_allHoops_full(find(wavelengths==RsquarePC_wave1),:) ./ GC_allHoops_full(find(wavelengths==RsquarePC_wave2),:);
%PC_Ratio = allHoops_waterCorrected_allData_noRmvl(find(wavelengths==RsquarePC_wave1),:) ./ allHoops_waterCorrected_allData_noRmvl(find(wavelengths==RsquarePC_wave2),:);
%PC_Ratio = allHoops_allData_noRmvl(find(wavelengths==RsquarePC_wave1),:) ./ allHoops_allData_noRmvl(find(wavelengths==RsquarePC_wave2),:);
%b = regress(BM_phycocyanin_content', [ones(size(PC_Ratio')) PC_Ratio']);
%b = regress(BM_phyco_EPIP', [ones(size(PC_Ratio')) PC_Ratio']);
%b = regress(BM_phyco_EPIL', [ones(size(PC_Ratio')) PC_Ratio']);
%b = regress(GC_08_17_phycocyanin_content', [ones(size(PC_Ratio')) PC_Ratio']);
%b = regress(GC_08_17_Phyco_EPIP', [ones(size(PC_Ratio')) PC_Ratio']);
%b = regress(GC_08_17_phyco_EPIL', [ones(size(PC_Ratio')) PC_Ratio']);
%b = regress(GC_09_09_phycocyanin_content', [ones(size(PC_Ratio')) PC_Ratio']);
%b = regress(GC_09_09_phyco_EPIP', [ones(size(PC_Ratio')) PC_Ratio']);
%b = regress(GC_09_09_phyco_EPIL', [ones(size(PC_Ratio')) PC_Ratio']);
%b = regress(GC_total_phycocyanin', [ones(size(PC_Ratio')) PC_Ratio']);
%b = regress(GC_phyco_epip', [ones(size(PC_Ratio')) PC_Ratio']);
b = regress(GC_phyco_epil', [ones(size(PC_Ratio')) PC_Ratio']);
%b = regress(GC_allPhyco_QA', [ones(size(PC_Ratio')) PC_Ratio']);
%b = regress(totalPhyco', [ones(size(PC_Ratio')) PC_Ratio']);
%b = regress(totalPhyco_EPIP', [ones(size(PC_Ratio')) PC_Ratio']);
%b = regress(totalPhyco_EPIL', [ones(size(PC_Ratio')) PC_Ratio']);

disp('  ')
disp('Phycocyanin Fit Chl-a')
disp(['     Maximum R-squared: ' num2str(Rsquare_PC)])
disp(['     Optimal band ratio: (' num2str(RsquarePC_wave1) '/' num2str(RsquarePC_wave2) ')'])
disp(['     X vs. d relation: d = ' num2str(b(1),4) ' + ' ...
    num2str(b(2),4) 'X'])

phycoPrediction = b(1) + b(2) .* PC_Ratio;
%rmse = sqrt(mean((BM_phycocyanin_content - phycoPrediction).^2))
%rmse = sqrt(mean((BM_phyco_EPIP - phycoPrediction).^2))
%rmse = sqrt(mean((BM_phyco_EPIL - phycoPrediction).^2))
%rmse = sqrt(mean((GC_08_17_phycocyanin_content - phycoPrediction).^2))
%rmse = sqrt(mean((GC_08_17_Phyco_EPIP - phycoPrediction).^2))
%rmse = sqrt(mean((GC_08_17_phyco_EPIL - phycoPrediction).^2))
%rmse = sqrt(mean((GC_09_09_phycocyanin_content - phycoPrediction).^2))
%rmse = sqrt(mean((GC_09_09_phyco_EPIP - phycoPrediction).^2))
%rmse = sqrt(mean((GC_09_09_phyco_EPIL - phycoPrediction).^2))
%rmse = sqrt(mean((GC_total_phycocyanin - phycoPrediction).^2))
%rmse = sqrt(mean((GC_phyco_epip - phycoPrediction).^2))
rmse = sqrt(mean((GC_phyco_epil - phycoPrediction).^2))
%rmse = sqrt(mean((GC_allPhyco_QA - phycoPrediction).^2))
%rmse = sqrt(mean((totalPhyco - phycoPrediction).^2))
%rmse = sqrt(mean((totalPhyco_EPIP - phycoPrediction).^2))
%rmse = sqrt(mean((totalPhyco_EPIL - phycoPrediction).^2))


% Plot
%nexttile
figure('position',[50 50 1600 1000])
%tiledlayout(1,2)
%plot(PC_Ratio, BM_phycocyanin_content,'o', 'linewidth', 2);
%plot(PC_Ratio, BM_phyco_EPIP,'o', 'linewidth', 2);
%plot(PC_Ratio, BM_phyco_EPIL,'o', 'linewidth', 2);
%plot(PC_Ratio, GC_08_17_phycocyanin_content,'o', 'linewidth', 2);
%plot(PC_Ratio, GC_08_17_Phyco_EPIP,'o', 'linewidth', 2);
%plot(PC_Ratio, GC_08_17_phyco_EPIL,'o', 'linewidth', 2);
%plot(PC_Ratio, GC_09_09_phycocyanin_content,'o', 'linewidth', 2);
%plot(PC_Ratio, GC_09_09_phyco_EPIP,'o', 'linewidth', 2);
%plot(PC_Ratio, GC_09_09_phyco_EPIL,'o', 'linewidth', 2);
%plot(PC_Ratio(1:7), GC_total_phycocyanin(1:7),'s', PC_Ratio(8:end), GC_total_phycocyanin(8:end),'d', 'linewidth', 2);
%plot(PC_Ratio(1:4), GC_phyco_epip(1:4),'o', PC_Ratio(5:end), GC_phyco_epip(5:end),'o', 'linewidth', 2);
plot(PC_Ratio(1:7), GC_phyco_epil(1:7),'o', PC_Ratio(8:end), GC_phyco_epil(8:end),'d', 'linewidth', 2);
%plot(PC_Ratio, GC_allPhyco_QA,'o', 'linewidth', 2);
%plot(PC_Ratio(1:7), totalPhyco(1:7),'o', PC_Ratio(8:26), totalPhyco(8:26),'s', PC_Ratio(27:end), totalPhyco(27:end),'d', 'linewidth', 2); legend('08-17 Gold Creek', '09-09 Gold Creek', '08-17 Bear Gulch');
%plot(PC_Ratio(1:4), totalPhyco_EPIP(1:4),'o', PC_Ratio(5:21), totalPhyco_EPIP(5:21),'s', PC_Ratio(22:end), totalPhyco_EPIP(22:end),'d', 'linewidth', 2); legend('08-17 Gold Creek','09-09 Gold Creek', '08-17 Bear Gulch');
%plot(PC_Ratio(1:7), totalPhyco_EPIL(1:7),'o', PC_Ratio(8:14), totalPhyco_EPIL(8:14),'s', PC_Ratio(15:end), totalPhyco_EPIL(15:end),'d', 'linewidth', 2); legend('08-17 Gold Creek', '08-17 Bear Gulch', '09-09 Gold Creek');
axLim = axis;
hold on
plot(axLim(1:2),b(1)+b(2).*axLim(1:2),'k-','linewidth',3);
axis(axLim);
text(0.1,0.9,char(['d = ' num2str(b(1),4) ' + ' num2str(b(2),4) 'X '],...
    ['R^2 = ' num2str(Rsquare_PC,4) '; RMSE = ' num2str(rmse,4) ' mg/m^2']),...
    'units','normalized','fontname','arial','fontsize',12)
xlabel(['Band Ratio (' num2str(RsquarePC_wave1,3) '/' num2str(RsquarePC_wave2,3) ')'])
ylabel('Phycocyanin Concentration [mg/m^2]')
title('Phycocyanin Concentration: Linear Model')

%%PLSR
if partialLeast == 1
    hoopData = GC_allHoops_waterCorrected_full;
    chlaData = GC_chla_filaEpip;
    PLSR(hoopData,chlaData)
else
end


%% All hoops (with sus points removed)
% if allHoops_on == 1
%     % All hoops version of above
%     Rsquare_allHoops = 0;
%     Rsquare_test = 0;
%     for n = 1:size(wavelengths,1)
%         allHoops_waterCorrected(n).LM(m).wavelength = wavelengths(n);
%         for m = 1:size(wavelengths,1)
%             allHoops_waterCorrected(n).LM(m).wavelength = wavelengths(m);
%             [~,~,~,~,stats] = regress(totalChla_allSites', [ones(size(allHoops_waterCorrected_ALL(n).num(m).den')) allHoops_waterCorrected_ALL(n).num(m).den']);
%             Rsquare_test = stats(1);
%             if Rsquare_test > Rsquare_allHoops
%                 Rsquare_allHoops = Rsquare_test;
%                 RsquareAllHoops_wave1 = wavelengths(n);
%                 RsquareAllHoops_wave2 = wavelengths(m);
%             else
%             end
%         end
%         fprintf('Building all hoops fit %.0f of %.0f \n', n, size(wavelengths,1))
%     end
% 
%     allHoopsRatio = allHoops_waterCorrect(find(wavelengths==RsquareAllHoops_wave1),:) ./ allHoops_waterCorrect(find(wavelengths==RsquareAllHoops_wave2),:);
%     b = regress(totalChla', [ones(size(allHoopsRatio')) allHoopsRatio']);
% 
%     disp('  ')
%     disp('Full Data (GC + BM) Fit Chl-a')
%     disp(['     Maximum R-squared: ' num2str(Rsquare_allHoops)])
%     disp(['     Optimal band ratio: (' num2str(RsquareAllHoops_wave1) '/' num2str(RsquareAllHoops_wave2) ')'])
%     disp(['     X vs. d relation: d = ' num2str(b(1),4) ' + ' ...
%         num2str(b(2),4) 'X'])
% 
%     chlaPrediction = b(1) + b(2) .* allHoopsRatio;
%     rmse = sqrt(mean((totalChla - chlaPrediction).^2));
% 
%     % Plot
%     figure('position',[50 50 1600 1000])
%     tiledlayout(1,2)
%     nexttile
%     plot(allHoopsRatio , totalChla,'o', 'LineWidth', 2);
%     axLim = axis;
%     hold on
%     plot(axLim(1:2),b(1)+b(2).*axLim(1:2),'k-','linewidth',2);
%     axis(axLim);
%     text(0.1,0.9,char(['d = ' num2str(b(1),4) ' + ' num2str(b(2),4) 'X '],...
%         ['R^2 = ' num2str(Rsquare_allHoops,4) '; RMSE = ' num2str(rmse,4) ' mg/m^2']),...
%         'units','normalized','fontname','arial','fontsize',12)
%     xlabel(['X = (' num2str(RsquareAllHoops_wave1,3) '/' num2str(RsquareAllHoops_wave2,3) ')'])
%     ylabel('Chl-a Concentration [mg/m^2]')
%     title('Chl-a Concentration: Linear Model (GC + BM)')
% 
%     % Phycocyanin version of above (all points, no sus removal)
%     Rsquare_PC_all = 0;
%     Rsquare_PCTest = 0;
%     for n = 1:size(wavelengths,1)
%         allHoops_waterCorrected_ALL(n).LM(m).wavelength = wavelengths(n);
%         for m = 1:size(wavelengths,1)
%             allHoops_waterCorrected_ALL(n).LM(m).wavelength = wavelengths(m);
%             [~,~,~,~,stats] = regress(totalPhyco', [ones(size(allHoops_waterCorrected_ALL(n).num(m).den')) allHoops_waterCorrected_ALL(n).num(m).den']);
%             Rsquare_PCTest = stats(1);
%             if Rsquare_PCTest > Rsquare_PC_all
%                 Rsquare_PC_all = Rsquare_PCTest;
%                 RsquarePC_all_wave1 = wavelengths(n);
%                 RsquarePC_all_wave2 = wavelengths(m);
%             else
%             end
%         end
%         fprintf('Building phycocyanin (FULL DATA) fit %.0f of %.0f \n', n, size(wavelengths,1))
%     end
% 
% 
%     PC_Ratio_all = allHoops_waterCorrected_allData_noRmvl(find(wavelengths==RsquarePC_all_wave1),:) ./ allHoops_waterCorrected_allData_noRmvl(find(wavelengths==RsquarePC_all_wave2),:);
%     b = regress(totalPhyco', [ones(size(PC_Ratio_all')) PC_Ratio_all']);
% 
%     disp('  ')
%     disp('Phycocyanin Fit Full Data')
%     disp(['     Maximum R-squared: ' num2str(Rsquare_PC_all)])
%     disp(['     Optimal band ratio: (' num2str(RsquarePC_all_wave1) '/' num2str(RsquarePC_all_wave2) ')'])
%     disp(['     X vs. d relation: d = ' num2str(b(1),4) ' + ' ...
%         num2str(b(2),4) 'X'])
% 
%     phycoPrediction = b(1) + b(2) .* PC_Ratio_all;
%     rmse = sqrt(mean((totalPhyco - phycoPrediction).^2))
% 
%     % Plot
%     %figure('position',[50 50 1600 800])
%     %tiledlayout(1,2)
%     nexttile
%     plot(PC_Ratio_all, totalPhyco,'o', 'linewidth', 2);
%     axLim = axis;
%     hold on
%     plot(axLim(1:2),b(1)+b(2).*axLim(1:2),'k-','linewidth',3);
%     axis(axLim);
%     text(0.1,0.9,char(['d = ' num2str(b(1),4) ' + ' num2str(b(2),4) 'X '],...
%         ['R^2 = ' num2str(Rsquare_PC_all,4) '; RMSE = ' num2str(rmse,4) ' mg/m^2']),...
%         'units','normalized','fontname','arial','fontsize',12)
%     xlabel(['Band Ratio (' num2str(RsquarePC_all_wave1,3) '/' num2str(RsquarePC_all_wave2,3) ')'])
%     ylabel('Phycocyanin Concentration [mg/m^2]')
%     title('Phycocyanin Concentration: Linear Model (GC + BM, no removal)')
% else
% end

if gradientOn == 1
    % Gradient version of above
    %     Rsquare_grad = 0;
    %     Rsquare_gradtest = 0;
    %     for n = 1:size(wavelengths,1)
    %         gradientAllHoops(n).LM(m).wavelength = wavelengths(n);
    %         for m = 1:size(wavelengths,1)
    %             gradientAllHoops(n).LM(m).wavelength = wavelengths(m);
    %             [~,~,~,~,stats] = regress(GC_total_chlorophyll_content_ritchie', [ones(size(gradientAllHoops(n).num(m).den')) gradientAllHoops(n).num(m).den']);
    %             Rsquare_test = stats(1);
    %             if Rsquare_test > Rsquare_grad
    %                 Rsquare_grad = Rsquare_test;
    %                 RsquareGrad_wave1 = wavelengths(n);
    %                 RsquareGrad_wave2 = wavelengths(m);
    %             else
    %             end
    %         end
    %         fprintf('Building gradient fit %.0f of %.0f \n', n, size(wavelengths,1))
    %     end

    Rsquare_grad = 0;
    Rsquare_gradtest = 0;
    for m = 1:size(wavelengths,1)
        gradientAllHoops(n).LM(m).wavelength = wavelengths(m);
        [~,~,~,~,stats] = regress(GC_total_chlorophyll_content_ritchie', [ones(size(GC_allHoops_gradient,2))' GC_allHoops_gradient(m,:)']);
        Rsquare_test = stats(1);
        if Rsquare_test > Rsquare_grad
            Rsquare_grad = Rsquare_test;
            %RsquareGrad_wave1 = wavelengths(n);
            RsquareGrad_wave2 = wavelengths(m);
        else
        end
        fprintf('Building gradient fit %.0f of %.0f \n', n, size(wavelengths,1))
    end

    %gradRatio = GC_allHoops_gradient(find(wavelengths==RsquareGrad_wave1),:) ./ GC_allHoops_gradient(find(wavelengths==RsquareGrad_wave2),:);
    gradRatio = GC_allHoops_gradient(find(wavelengths==RsquareGrad_wave2),:);
    b = regress(GC_total_chlorophyll_content_ritchie', [ones(size(gradRatio')) gradRatio']);

    disp('  ')
    disp('Gradient Fit Chl-a')
    disp(['     Maximum R-squared: ' num2str(Rsquare_grad)])
    disp(['     Optimal band ratio: (' num2str(RsquareGrad_wave1) '/' num2str(RsquareGrad_wave2) ')'])
    disp(['     X vs. d relation: d = ' num2str(b(1),4) ' + ' ...
        num2str(b(2),4) 'X'])

    chlaPrediction = b(1) + b(2) .* gradRatio;
    rmse = sqrt(mean((GC_total_chlorophyll_content_ritchie - chlaPrediction).^2));

    % Plot
    figure('position',[50 50 1600 1000])
    plot(gradRatio, GC_total_chlorophyll_content_ritchie,'b.');
    axLim = axis;
    hold on
    plot(axLim(1:2),b(1)+b(2).*axLim(1:2),'k-','linewidth',2);
    axis(axLim);
    text(0.1,0.9,char(['d = ' num2str(b(1),4) ' + ' num2str(b(2),4) 'X '],...
        ['R^2 = ' num2str(Rsquare_grad,4) '; RMSE = ' num2str(rmse,4) ' mg/m^2']),...
        'units','normalized','fontname','arial','fontsize',12)
    xlabel(['X = (' num2str(RsquareGrad_wave1,3) '/' num2str(RsquareGrad_wave2,3) ')'])
    ylabel('Chl-a Concentration [mg/m^2]')
    title('Gradient Model')
else
end

if sub100_on == 1
    % Separating my chl-a concentration (Simple ratio, sub 100)
    Rsquare_sub100 = 0;
    Rsquare_test = 0;
    for n = 1:size(wavelengths,1)
        AllHoopsSub100(n).LM(m).wavelength = wavelengths(n);
        for m = 1:size(wavelengths,1)
            AllHoopsSub100(n).LM(m).wavelength = wavelengths(m);
            [~,~,~,~,stats] = regress(Total_chla_sub100', [ones(size(AllHoopsSub100(n).num(m).den')) AllHoopsSub100(n).num(m).den']);
            Rsquare_test = stats(1);
            if Rsquare_test > Rsquare_sub100
                Rsquare_sub100 = Rsquare_test;
                RsquareSub100_wave1 = wavelengths(n);
                RsquareSub100_wave2 = wavelengths(m);
            else
            end
        end
        fprintf('Building Sub-100 fit %.0f of %.0f \n', n, size(wavelengths,1))
    end

    linearRatio = allHoops_sub100(find(wavelengths==RsquareSub100_wave1),:)./allHoops_sub100(find(wavelengths==RsquareSub100_wave2),:);
    b = regress(Total_chla_sub100', [ones(size(linearRatio')) linearRatio']);

    disp('  ')
    disp('Linear Fit Sub-100 Chl-a')
    disp(['     Maximum R-squared: ' num2str(Rsquare_sub100)])
    disp(['     Optimal band ratio: (' num2str(RsquareSub100_wave1) '/' num2str(RsquareSub100_wave2) ')'])
    disp(['     X vs. d relation: d = ' num2str(b(1),4) ' + ' ...
        num2str(b(2),4) 'X'])

    chlaPrediction = b(1)+b(2).*linearRatio;
    rmse = sqrt(mean((Total_chla_sub100 - chlaPrediction).^2));

    % Plot
    figure('position',[50 50 1600 1000])
    plot(linearRatio , Total_chla_sub100,'o', 'LineWidth', 2);
    axLim = axis;
    hold on
    plot(axLim(1:2),b(1)+b(2)*axLim(1:2),'k-','linewidth',2);
    axis(axLim);
    text(0.1,0.9,char(['d = ' num2str(b(1),4) ' + ' num2str(b(2),4) 'X '],...
        ['R^2 = ' num2str(Rsquare_sub100,4) '; RMSE = ' num2str(rmse,4) ' mg/m^2']),...
        'units','normalized','fontname','arial','fontsize',12)
    xlabel(['X = (' num2str(RsquareSub100_wave1,3) '/' num2str(RsquareSub100_wave2,3) ')'])
    ylabel('Chl-a Concentration [mg/m^2]')
    title('Linear Model')
else
end

if tween100200_on == 1
    % Separating my chl-a concentration (Simple ratio, sub 150)
    Rsquare_100200 = 0;
    Rsquare_test = 0;
    for n = 1:size(wavelengths,1)
        AllHoops_100200(n).LM(m).wavelength = wavelengths(n);
        for m = 1:size(wavelengths,1)
            AllHoops_100200(n).LM(m).wavelength = wavelengths(m);
            [~,~,~,~,stats] = regress(Total_chla_100200', [ones(size(AllHoops_100200(n).num(m).den')) AllHoops_100200(n).num(m).den']);
            Rsquare_test = stats(1);
            if Rsquare_test > Rsquare_100200
                Rsquare_100200 = Rsquare_test;
                Rsquare_100200_wave1 = wavelengths(n);
                Rsquare_100200_wave2 = wavelengths(m);
            else
            end
        end
        fprintf('Building 100-200 fit %.0f of %.0f \n', n, size(wavelengths,1))
    end

    linearRatio = allHoops_100200(find(wavelengths==Rsquare_100200_wave1),:)./allHoops_100200(find(wavelengths==Rsquare_100200_wave2),:);
    b = regress(Total_chla_100200', [ones(size(linearRatio')) linearRatio']);

    disp('  ')
    disp('Linear Fit 100-200 Chl-a')
    disp(['     Maximum R-squared: ' num2str(Rsquare_100200)])
    disp(['     Optimal band ratio: (' num2str(Rsquare_100200_wave1) '/' num2str(Rsquare_100200_wave2) ')'])
    disp(['     X vs. d relation: d = ' num2str(b(1),4) ' + ' ...
        num2str(b(2),4) 'X'])

    chlaPrediction = b(1)+b(2).*linearRatio;
    rmse = sqrt(mean((Total_chla_100200 - chlaPrediction).^2));

    % Plot
    figure('position',[50 50 1600 1000])
    plot(linearRatio , Total_chla_100200,'o', 'LineWidth', 2);
    axLim = axis;
    hold on
    plot(axLim(1:2),b(1)+b(2)*axLim(1:2),'k-','linewidth',2);
    axis(axLim);
    text(0.1,0.9,char(['d = ' num2str(b(1),4) ' + ' num2str(b(2),4) 'X '],...
        ['R^2 = ' num2str(Rsquare_100200,4) '; RMSE = ' num2str(rmse,4) ' mg/m^2']),...
        'units','normalized','fontname','arial','fontsize',12)
    xlabel(['X = (' num2str(Rsquare_100200_wave1,3) '/' num2str(Rsquare_100200_wave2,3) ')'])
    ylabel('Chl-a Concentration [mg/m^2]')
    title('Linear Model')
else
end

% if sub200_on == 1
%     % Separating my chl-a concentration (Simple ratio, sub 200)
%     Rsquare_sub200 = 0;
%     Rsquare_test = 0;
%     for n = 1:size(wavelengths,1)
%         AllHoopsSub200(n).LM(m).wavelength = wavelengths(n);
%         for m = 1:size(wavelengths,1)
%             AllHoopsSub200(n).LM(m).wavelength = wavelengths(m);
%             [~,~,~,~,stats] = regress(GC_total_chla_sub200', [ones(size(AllHoopsSub200(n).num(m).den')) AllHoopsSub200(n).num(m).den']);
%             Rsquare_test = stats(1);
%             if Rsquare_test > Rsquare_sub200
%                 Rsquare_sub200 = Rsquare_test;
%                 RsquareSub200_wave1 = wavelengths(n);
%                 RsquareSub200_wave2 = wavelengths(m);
%             else
%             end
%         end
%         fprintf('Building Sub-200 fit %.0f of %.0f \n', n, size(wavelengths,1))
%     end
% 
%     linearRatio = GC_allHoops_sub200(find(wavelengths==RsquareSub200_wave1),:)./GC_allHoops_sub200(find(wavelengths==RsquareSub200_wave2),:);
%     b = regress(GC_total_chla_sub200', [ones(size(linearRatio')) linearRatio']);
% 
%     disp('  ')
%     disp('Linear Fit Sub-200 Chl-a')
%     disp(['     Maximum R-squared: ' num2str(Rsquare_sub200)])
%     disp(['     Optimal band ratio: (' num2str(RsquareSub200_wave1) '/' num2str(RsquareSub200_wave2) ')'])
%     disp(['     X vs. d relation: d = ' num2str(b(1),4) ' + ' ...
%         num2str(b(2),4) 'X'])
% 
%     chlaPrediction = b(1)+b(2).*linearRatio;
%     rmse = sqrt(mean((GC_total_chla_sub200 - chlaPrediction).^2));
% 
%     % Plot
%     figure('position',[50 50 1600 1000])
%     plot(linearRatio, GC_total_chla_sub200,'o','LineWidth',2);
%     axLim = axis;
%     hold on
%     plot(axLim(1:2),b(1)+b(2)*axLim(1:2),'k-','linewidth',2);
%     axis(axLim);
%     text(0.1,0.9,char(['d = ' num2str(b(1),4) ' + ' num2str(b(2),4) 'X '],...
%         ['R^2 = ' num2str(Rsquare_sub200,4) '; RMSE = ' num2str(rmse,4) ' mg/m^2']),...
%         'units','normalized','fontname','arial','fontsize',12)
%     xlabel(['X = (' num2str(RsquareSub200_wave1,3) '/' num2str(RsquareSub200_wave2,3) ')'])
%     ylabel('Chl-a Concentration [mg/m^2]')
%     title('Linear Model')
% else
% end

if plus200_on == 1
    % Separating my chl-a concentration (Simple ratio, plus 200)
    Rsquare_plus200 = 0;
    Rsquare_test = 0;
    for n = 1:size(wavelengths,1)
        AllHoopsPlus200(n).LM(m).wavelength = wavelengths(n);
        for m = 1:size(wavelengths,1)
            AllHoopsPlus200(n).LM(m).wavelength = wavelengths(m);
            [~,~,~,~,stats] = regress(Total_chla_plus200', [ones(size(AllHoopsPlus200(n).num(m).den')) AllHoopsPlus200(n).num(m).den']);
            Rsquare_test = stats(1);
            if Rsquare_test > Rsquare_plus200
                Rsquare_plus200 = Rsquare_test;
                RsquarePlus200_wave1 = wavelengths(n);
                RsquarePlus200_wave2 = wavelengths(m);
            else
            end
        end
        fprintf('Building Plus-200 fit %.0f of %.0f \n', n, size(wavelengths,1))
    end

    linearRatio = allHoops_plus200(find(wavelengths==RsquarePlus200_wave1),:)./allHoops_plus200(find(wavelengths==RsquarePlus200_wave2),:);
    b = regress(Total_chla_plus200', [ones(size(linearRatio')) linearRatio']);

    disp('  ')
    disp('Linear Fit Plus-200 Chl-a')
    disp(['     Maximum R-squared: ' num2str(Rsquare_plus200)])
    disp(['     Optimal band ratio: (' num2str(RsquarePlus200_wave1) '/' num2str(RsquarePlus200_wave2) ')'])
    disp(['     X vs. d relation: d = ' num2str(b(1),4) ' + ' ...
        num2str(b(2),4) 'X'])

    chlaPrediction = b(1)+b(2).*linearRatio;
    rmse = sqrt(mean((Total_chla_plus200 - chlaPrediction).^2));

    % Plot
    figure('position',[50 50 1600 1000])
    plot(linearRatio, Total_chla_plus200,'o', 'LineWidth',2);
    axLim = axis;
    hold on
    plot(axLim(1:2),b(1)+b(2)*axLim(1:2),'k-','linewidth',2);
    axis(axLim);
    text(0.1,0.9,char(['d = ' num2str(b(1),4) ' + ' num2str(b(2),4) 'X '],...
        ['R^2 = ' num2str(Rsquare_plus200,4) '; RMSE = ' num2str(rmse,4) ' mg/m^2']),...
        'units','normalized','fontname','arial','fontsize',12)
    xlabel(['X = (' num2str(RsquarePlus200_wave1,3) '/' num2str(RsquarePlus200_wave2,3) ')'])
    ylabel('Chl-a Concentration [mg/m^2]')
    title('Linear Model for Chl-a Above 200 mg/m^2')
else
end

%% Three band stuff

if threeBand_on == 1
    % All hoops version of above
    Rsquare_allHoops_threeBand = 0;
    Rsquare_test = 0;
    for n = 1:size(wavelengths,1)
        for m = 1:size(wavelengths,1)
            for p = 1:size(wavelengths,1)
            [~,~,~,~,stats] = regress(totalChla', [ones(size(AllHoops_threeBand(n).num(m).den(p).multiplier')) AllHoops_threeBand(n).num(m).den(p).multiplier']);
            Rsquare_test = stats(1);
            if Rsquare_test > Rsquare_allHoops_threeBand
                Rsquare_allHoops_threeBand = Rsquare_test;
                RsquareAllHoops_threeBand_wave1 = wavelengths(n);
                RsquareAllHoops_threeBand_wave2 = wavelengths(m);
                RsquareAllHoops_threeBand_wave3 = wavelengths(p);
            else
            end
            end
        end
        fprintf('Building all hoops fit %.0f of %.0f \n', n, size(wavelengths,1))
    end

    allHoopsRatio_threeBand = ( 1./allHoops_waterCorrect(find(wavelengths==RsquareAllHoops_threeBand_wave1),:) - 1./allHoops_waterCorrect(find(wavelengths==RsquareAllHoops_threeBand_wave2),:) ) .* allHoops_waterCorrect(find(wavelengths==RsquareAllHoops_threeBand_wave3),:) ;
    b = regress(totalChla', [ones(size(allHoopsRatio_threeBand')) allHoopsRatio_threeBand']);

    disp('  ')
    disp('Full Data (GC + BM) Fit Chl-a')
    disp(['     Maximum R-squared: ' num2str(Rsquare_allHoops_threeBand)])
    disp(['     Optimal band ratio: (1/' num2str(RsquareAllHoops_wave1) '- 1/' num2str(RsquareAllHoops_wave2) ') * ' num2str(RsquareAllHoops_wave3) ])
    disp(['     X vs. d relation: d = ' num2str(b(1),4) ' + ' ...
        num2str(b(2),4) 'X'])

    chlaPrediction = b(1) + b(2) .* allHoopsRatio_threeBand;
    rmse = sqrt(mean((totalChla - chlaPrediction).^2));

    % Plot
    figure('position',[50 50 1600 1000])
    tiledlayout(1,2)
    nexttile
    plot(allHoopsRatio_threeBand, totalChla,'o', 'LineWidth', 2);
    axLim = axis;
    hold on
    plot(axLim(1:2),b(1)+b(2).*axLim(1:2),'k-','linewidth',2);
    axis(axLim);
    text(0.1,0.9,char(['d = ' num2str(b(1),4) ' + ' num2str(b(2),4) 'X '],...
        ['R^2 = ' num2str(Rsquare_allHoops_threeBand,4) '; RMSE = ' num2str(rmse,4) ' mg/m^2']),...
        'units','normalized','fontname','arial','fontsize',12)
    %xlabel(['X = (' num2str(RsquareAllHoops_wave1,3) '/' num2str(RsquareAllHoops_wave2,3) ')'])
    ylabel('Chl-a Concentration [mg/m^2]')
    title('Chl-a Concentration: Linear Model (GC + BM)')
else
end

%% Display k most highly correlated wavelengths pairs and associated r2 vals for chl-a
zeroPoints = size(RsquareLinearFull,1) - nnz(RsquareLinearFull.R2);

k = size(RsquareLinearFull,1)-zeroPoints;
r2Vals = array2table(r2Values(RsquareLinearFull, k), 'VariableNames',{'R2', 'Wavelength1', 'Wavelength2'});

figure
box on
scatter(r2Vals.Wavelength1, r2Vals.Wavelength2, 35*abs(r2Vals.R2), r2Vals.R2, 'filled', 'MarkerFaceAlpha', 0.6);
xlabel('Wavelength [nm]')
ylabel('Wavelength [nm]')
ax = gca;
colorbar(ax);
ax.Visible = 'on';
%ax.Position(4) = ax.Position(4)*0.9;
ax.XLim = [400 850];
ax.YLim = [400 850];
%axis(h, 'equal')
colormap('jet')
title('Correlation Map | Chlorophyll-a')
box on

%% Display k most highly correlated wavelengths pairs and associated r2 vals for phycocyanin
zeroPoints = size(RsquarePCFull,1) - nnz(RsquarePCFull.R2);

k = size(RsquarePCFull,1)-zeroPoints;
r2Vals = array2table(r2Values(RsquarePCFull, k), 'VariableNames',{'R2', 'Wavelength1', 'Wavelength2'});

figure
box on
scatter(r2Vals.Wavelength1, r2Vals.Wavelength2, 35.*abs(r2Vals.R2), r2Vals.R2, 'filled', 'MarkerFaceAlpha', 0.6);
xlabel('Wavelength [nm]')
ylabel('Wavelength [nm]')
ax = gca;
colorbar(ax);
ax.Visible = 'on';
%ax.Position(4) = ax.Position(4)*0.9;
ax.XLim = [400 850];
ax.YLim = [400 850];
%axis(h, 'equal')
colormap('jet')
title('Correlation Map | Phycocyanin')
box on

% %Sort rows of table
% r2ValsSorted = sortrows(r2Vals,2);
%
% % Labels for scatter plot
% labelsX = (r2ValsSorted.Wavelength1) + "nm";
% labelsY = (r2ValsSorted.Wavelength2) + "nm";
%
% figure
% n = size(r2Vals, 1);
% y = triu(repmat(n+1, n, n) - (1:n)') + 0.5;
% x = triu(repmat(1:n, n, 1)) + 0.5;
% x(x == 0.5) = NaN;
% r2ValsBIG = repmat(r2ValsSorted.R2, 1, size(r2Vals,1));
% scatter(x(:), y(:), 100.*abs(r2ValsBIG(:)), r2ValsBIG(:), 'filled', 'MarkerFaceAlpha', 0.6)
%
% % enclose markers in a grid
% xl = [1:n+1;repmat(n+1, 1, n+1)];
% xl = [xl(:, 1), xl(:, 1:end-1)];
% yl = repmat(n+1:-1:1, 2, 1);
% line(xl, yl, 'color', 'k') % horizontal lines
% line(yl, xl, 'color', 'k') % vertical lines
%
% % show labels
% text(1:n, (n:-1:1) + 0.5, labelsX, 'HorizontalAlignment', 'right')
%
% text((1:n) + 0.5, repmat(n + 1, n, 1), labelsY, ...
%     'HorizontalAlignment', 'right', 'Rotation', 270)
%
% %Plot details
% h = gca;
% colorbar(h);
% h.Visible = 'off';
% h.Position(4) = h.Position(4)*0.9;
% axis(h, 'equal')
% colormap('jet')

%% Compare to other indices?

% Use these

% Grab relfectance data from 778 through 886 nm
NDVI_sentinel_data1 = sum(allHoops_waterCorrected_allData_noRmvl(182:214,:));
% Grab relfectance data from 649 through 679 nm
NDVI_sentinel_data2 = sum(allHoops_waterCorrected_allData_noRmvl(121:136,:));

NDVI_sentinel = (NDVI_sentinel_data1 - NDVI_sentinel_data2) ./ (NDVI_sentinel_data1 + NDVI_sentinel_data2);

figure
plot(NDVI_sentinel, totalChla_allSites,'o');

NCI = ( (allHoops_waterCorrected_allData_noRmvl(find(wavelengths==690.54),:) ./ allHoops_waterCorrected_allData_noRmvl(find(wavelengths==549.98),:)) - ( allHoops_waterCorrected_allData_noRmvl(find(wavelengths==675.67),:) / allHoops_waterCorrected_allData_noRmvl(find(wavelengths==699.05),:)) ) ./ ...
    ( (allHoops_waterCorrected_allData_noRmvl(find(wavelengths==690.54),:) ./ allHoops_waterCorrected_allData_noRmvl(find(wavelengths==549.98),:)) + ( allHoops_waterCorrected_allData_noRmvl(find(wavelengths==675.67),:) / allHoops_waterCorrected_allData_noRmvl(find(wavelengths==699.05),:)) );

figure
plot(NCI, totalChla_allSites,'o');

%%%
% chla_ratioClassic = allHoops_waterCorrected_allData_noRmvl(find(wavelengths==709.71),:) ./ allHoops_waterCorrected_allData_noRmvl(find(wavelengths==665.08),:);
% figure
% plot(chla_ratioClassic, totalChla_allSites,'o');(legend('fit to me'));
% hold on
% plot(chla_ratioClassic(1:7), totalChla_allSites(1:7), 'or', chla_ratioClassic(8:25), totalChla_allSites(8:25), 'db', chla_ratioClassic(26:end), totalChla_allSites(26:end), 'sg', "LineWidth",2); legend('08-17 Gold Creek', '09-09 Gold Creek', '08-17 Bear Gulch');
% 
% figure
% plot(chla_ratioClassic, Chla_allSites_filaEpip,'o');(legend('fit to me'));
% hold on
% plot(chla_ratioClassic(1:7), Chla_allSites_filaEpip(1:7), 'or', chla_ratioClassic(8:25), Chla_allSites_filaEpip(8:25), 'db', chla_ratioClassic(26:end), Chla_allSites_filaEpip(26:end), 'sg', "LineWidth",2); legend('08-17 Gold Creek', '09-09 Gold Creek', '08-17 Bear Gulch');
% ylabel('Chl-a Concentration [mg/m^2]');
% xlabel('Band Ratio 709/665');
% 
% NDVI = (allHoops_waterCorrected_allData_noRmvl(find(wavelengths==750.40),:) - allHoops_waterCorrected_allData_noRmvl(find(wavelengths==682.04),:)) ./ (allHoops_waterCorrected_allData_noRmvl(find(wavelengths==750.40),:) + allHoops_waterCorrected_allData_noRmvl(find(wavelengths==682.04),:));
% figure
% plot(NDVI, totalChla_allSites,'o');(legend('fit to me'));
% hold on
% plot(NDVI(1:7), totalChla_allSites(1:7), 'or', NDVI(8:25), totalChla_allSites(8:25), 'db', NDVI(26:end), totalChla_allSites(26:end), 'sg', "LineWidth",2); legend('08-17 Gold Creek', '09-09 Gold Creek', '08-17 Bear Gulch');
% 
% figure
% plot(NDVI, Chla_allSites_filaEpip,'o');(legend('fit to me'));
% hold on
% plot(NDVI(1:7), Chla_allSites_filaEpip(1:7), 'or', NDVI(8:25), Chla_allSites_filaEpip(8:25), 'db', NDVI(26:end), Chla_allSites_filaEpip(26:end), 'sg', "LineWidth",2); legend('08-17 Gold Creek', '09-09 Gold Creek', '08-17 Bear Gulch');
% ylabel('Chl-a Concentration [mg/m^2]');
% xlabel('NDVI');
% 
% %figure
% %plot(totalChla_allSites, classicRatio,'o');
% %plot(Chla_allSites_filaEpip, classicRatio,'o');
% 
% NDVI = (allHoops_waterCorrected_allData_noRmvl(find(wavelengths==750.40),:) - allHoops_waterCorrected_allData_noRmvl(find(wavelengths==682.04),:)) ./ (allHoops_waterCorrected_allData_noRmvl(find(wavelengths==750.40),:) + allHoops_waterCorrected_allData_noRmvl(find(wavelengths==682.04),:));
% figure
% plot(totalChla_allSites, NDVI,'o');
% 
% figure;
% plot(Chla_allSites_filaEpip, NDVI, 'o')
% 
% figure;
% plot(Chla_allSites_epil, NDVI, 'o')

