%% Line height analysis function for use with chlaRegression.m

% Riley Logan
% Summer 2022

%function[] = LHA(wavelengths, hoopData)

%% Build all possible combinations of the line height analysis equation
rVal = 2;
w1 = 1;
w2 = 1;
w3 = 1;
RsquareLHA= array2table(zeros(size(nchoosek(wavelengths,2),1),4), "VariableNames",{'R2', 'Band 1', 'Band 2','Band 3'});
LHA_build(214).band1(214).band2(214).band3 = zeros();

for band1 = 1:size(wavelengths,1)
    for band2 = 1:size(wavelengths,1)
        for band3 = 1:size(wavelengths,1)

            LHA_build(band1).band1(band2).band2(band3).band3 =  abs(( (wavelengths(band2,:)-wavelengths(band1,:)).*(hoopData(band3,:)-hoopData(band1,:)) ./ ...
                (wavelengths(band3,:)-wavelengths(band1,:) + 0.001 ) + (hoopData(band1,:) - hoopData(band2,:))));

            [b,bint,r,rint,stats] = regress(GC_chla_filaEpip', [ones(size(LHA_build(band1).band1(band2).band2(band3).band3')) LHA_build(band1).band1(band2).band2(band3).band3']);

            Rsquare_test = stats(1);

            if Rsquare_test > RsquareLHA.R2(rVal-1)

                RsquareLHA(rVal, 'R2') = {stats(1)};

                RsquareLHA(w1, 'Band 1') = {wavelengths(band1)};
                RsquareLHA(w2, 'Band 2') = {wavelengths(band2)};
                RsquareLHA(w3, 'Band 3') = {wavelengths(band3)};

                rVal = rVal+1;
                w1 = w1+1;
                w2 = w2+1;
                w3 = w3+1;
                w4 = w4+1;
                w5 = w5+1;
                w6 = w6+1;
            else
            end

        end
    end
    fprintf('Building LHA %.0f of %.0f \n', band1, size(wavelengths,1))
end

plot(GC_chla_filaEpip, abs(( (wavelengths(band2,:)-wavelengths(band1,:)).*(hoopData(band3,:)-hoopData(band1,:)) ./ (wavelengths(band3,:)-wavelengths(band1,:) + 0.001 ) + (hoopData(band1,:) - hoopData(band2,:)))),'o' );