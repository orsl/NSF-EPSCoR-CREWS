%% Generate functions to compare regression performance
% Relies on "regression_clean.m

% Riley Logan
% August 2022


%% Function blah blah

pigment = updatedBiomass.FILA_CHLA_MG_M2_HAUER(1:34) + updatedBiomass.EPIP_CHLA_MG_M2_HAUER(1:34) + updatedBiomass.EPIL_CHLA_MG_M2_HAUER(1:34);

roundedWaves = round(wavelengths);

totalIntegral = zeros(size(hoopData2021,1),size(hoopData2021,2)+1);
totalIntegral(:,1) = roundedWaves; 
for m = 1:size(hoopData2021,2)
    totalIntegral(:,m+1) = cumtrapz(wavelengths,hoopData2021(:,m));
end

%% NDVI defined as 650 +- 65 nm in the red, 810 +- 65 in the NIR by Apogee systems (have study showing sensitivity to leaf chlorophyll content

NDVI_numer = zeros(size(hoopData2021,2),1)';
NDVI_denom = zeros(size(hoopData2021,2),1)';

NDVI_numer = ( totalIntegral(find(roundedWaves == 650+34),2:end) - totalIntegral(find(roundedWaves == 650-33),2:end) ) - (totalIntegral(find(roundedWaves == 810+34),2:end) - totalIntegral(find(roundedWaves == 810-32),2:end)); 
NDVI_denom = ( totalIntegral(find(roundedWaves == 650+34),2:end) - totalIntegral(find(roundedWaves == 650-33),2:end) ) + (totalIntegral(find(roundedWaves == 810+34),2:end) - totalIntegral(find(roundedWaves == 810-32),2:end));

NDVI = NDVI_numer./NDVI_denom;

figure
plot(NDVI, pigment, 'o', NDVI(1:7),pigment(1:7),'or', NDVI(15:34), pigment(15:34), 'db', NDVI(8:14), pigment(8:14), 'gs');
xlabel('NDVI');
ylabel('Chlorophyll a Concentration [mg/m^2]')
legend('total','08-17 Gold Creek', '09-09 Gold Creek', '08-17 Bear Gulch');

%% Generate index referenced in Shafique et al (2003) which uses 705/675
% with 5-nm resolution
shafiqueNumer = zeros(size(hoopData2021,2),1)';
shafiqueDenom = zeros(size(hoopData2021,2),1)';

shafiqueNumer = totalIntegral(find(roundedWaves == 708),2:end) - totalIntegral(find(roundedWaves == 703),2:end); 
shafiqueDenom = totalIntegral(find(roundedWaves == 678),2:end) - totalIntegral(find(roundedWaves == 671),2:end);

shafiqueIndex = shafiqueNumer./shafiqueDenom;

figure
plot(shafiqueIndex, pigment, 'o', shafiqueIndex(1:7),pigment(1:7),'or', shafiqueIndex(15:34),pigment(15:34),'db', shafiqueIndex(8:14),pigment(8:14),'gs');
xlabel('Shafique Index');
ylabel('Chlorophyll a Concentration [mg/m^2]')
legend('total', '08-17 Gold Creek', '09-09 Gold Creek', '08-17 Bear Gulch');

%% Generate index referenced in Ha et al (2017) which uses 559.8/664.6
% with 30- and 35-nm resolution, respectively

haNumer = zeros(size(hoopData2021,2),1)';
haDenom = zeros(size(hoopData2021,2),1)';

haNumer = totalIntegral(find(roundedWaves == 560+15),2:end) - totalIntegral(find(roundedWaves == 560-16),2:end); 
haDenom = totalIntegral(find(roundedWaves == 665+17),2:end) - totalIntegral(find(roundedWaves == 665-17),2:end);

haIndex = haNumer./haDenom;

figure
plot(haIndex, pigment, 'o');
hold on
plot(haIndex(1:7) ,pigment(1:7),'or', haIndex(15:34) ,pigment(15:34),'db', haIndex(8:14) ,pigment(8:14),'gs');
xlabel('Ha Index');
ylabel('Chlorophyll a Concentration [mg/m^2]')
legend('08-17 Gold Creek', '09-09 Gold Creek', '08-17 Bear Gulch');

