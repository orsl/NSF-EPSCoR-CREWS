% Load water attenuation profiles

function [waterPathLoss] = waterAttenuation(depth)

waterDir = 'C:/Users/Riley/Documents/1_Projects/NSF CREWS/2021/Data/Water Attenuation/';
attenProfiles = zeros(size(dir(fullfile(waterDir, '*.txt'))));
attenProfiles = dir(fullfile(waterDir, '*.txt'));
for n = 1:size(attenProfiles,1)
    attenProfiles(n).Data = readtable(fullfile(attenProfiles(n).folder, attenProfiles(n).name));
end

% Data for 5 cm On 22 July seems suspect (outlier) --remove--
%extinctionCoeff_5cm_r1 = real(-log(attenProfiles(6).Data.Var2 ./ attenProfiles(7).Data.Var2) ./ 0.05);
extinctionCoeff_10cm_r1 = real(-log(attenProfiles(1).Data.Var2 ./ attenProfiles(7).Data.Var2) ./ 0.10);
extinctionCoeff_15cm_r1 = real(-log(attenProfiles(2).Data.Var2 ./ attenProfiles(7).Data.Var2) ./ 0.15);
extinctionCoeff_20cm_r1 = real(-log(attenProfiles(3).Data.Var2 ./ attenProfiles(7).Data.Var2) ./ 0.20);
extinctionCoeff_30cm_r1 = real(-log(attenProfiles(4).Data.Var2 ./ attenProfiles(7).Data.Var2) ./ 0.30);
extinctionCoeff_40cm_r1 = real(-log(attenProfiles(5).Data.Var2 ./ attenProfiles(7).Data.Var2) ./ 0.40);

extinctionCoeff_10cm_r2 = real(-log(attenProfiles(8).Data.Var2 ./ attenProfiles(13).Data.Var2) ./ 0.10);
extinctionCoeff_20cm_r2 = real(-log(attenProfiles(9).Data.Var2 ./ attenProfiles(13).Data.Var2) ./ 0.20);
extinctionCoeff_30cm_r2 = real(-log(attenProfiles(10).Data.Var2 ./ attenProfiles(13).Data.Var2) ./ 0.30);
extinctionCoeff_40cm_r2 = real(-log(attenProfiles(11).Data.Var2 ./ attenProfiles(13).Data.Var2) ./ 0.40);
extinctionCoeff_50cm_r2 = real(-log(attenProfiles(12).Data.Var2 ./ attenProfiles(13).Data.Var2) ./ 0.50);

extinctionCoeff_total = [extinctionCoeff_10cm_r1, extinctionCoeff_15cm_r1, extinctionCoeff_20cm_r1, extinctionCoeff_30cm_r1, extinctionCoeff_40cm_r1,...
    extinctionCoeff_10cm_r2, extinctionCoeff_20cm_r2, extinctionCoeff_30cm_r2, extinctionCoeff_40cm_r2, extinctionCoeff_50cm_r2];

order = 4;
framelen = 5;
aveExtinction = sgolayfilt(mean(extinctionCoeff_total, 2), order, framelen);

extinctionLoss =  exp(-aveExtinction(190:3550,:).*depth);

waterPathLoss = [attenProfiles(1).Data.Var1(190:3550), (1-extinctionLoss).^2];

% figure("Position", [50 50 1600 1000])
% for n = 1:size(attenProfiles,1)
%     plot(attenProfiles(1).Data.Var1, attenProfiles(n).Data.Var2)
%     hold on
% end
% ylim([0 2^16])%max(max(extinctionCoeff_total))])
% xlim([400 1000])
% xlabel('Wavelength [nm]')
% ylabel('Digital Number [counts]')
% title('Unprocessed OceanOptics Measurements from 8 and 22 July 2021')
% legend('22 July - 10cm', '22 July - 15cm', '22 July - 20cm','22 July - 30cm', '22 July - 40cm','22 July - 5cm',...
% '22 July - Surface', '8 July - 10cm', '8 July - 20cm', '8 July - 30cm', '8 July - 40cm', '8 July - 50cm', '8 July - Surface')
% 
% figure("Position", [50 50 1600 1000])
% for n = 1:size(extinctionCoeff_total,2)
%     plot(attenProfiles(1).Data.Var1, movmean(sgolayfilt(real(extinctionCoeff_total(:,n)), order, framelen),50))
%     hold on
% end
% hold on
% plot(attenProfiles(1).Data.Var1, movmean(aveExtinction,50), 'LineWidth', 3)
% %plot(attenProfiles(1).Data.Var1, aveExtinction.*2, 'LineWidth', 4)
% %ylim([0 100])%max(max(extinctionCoeff_total))])
% xlim([400 950])
% xlabel('Wavelength [nm]')
% ylabel('Extinction Coefficient [1/m^2]')
% %title('Absorption Coefficients from 8 and 22 July 2021')
% legend('Spectrometer Measurements', 'Average')

% figure
% plot(attenProfiles(1).Data.Var1(190:3550), (1-extinctionLoss).^2)
