%% Analyze by normalizing to bottom of 664 trough then measuring line heights of green and red edge

% Riley Logan
% August 2022


%% function blah blah
% 

figure
for m = 7:15
    txt = ['chla conc = ', num2str(pigment2analyze(m))];
    plot(wavelengths,hoopData(:,m),'DisplayName',txt);
    legend show
    hold on
    w = waitforbuttonpress;
end

chlaConcentration = pigment2analyze;
chlaTroughIndex = zeros(size(hoopData,2),1);
maxGreen = zeros(size(hoopData,2),1);
greenIndex = zeros(size(hoopData,2),1);
maxRedEdge = zeros(size(hoopData,2),1);
redEdgeIndex = zeros(size(hoopData,2),1);

for m = 1:size(hoopData,2)
    [chlaTrough, chlaTroughIndex] = min(hoopData(129:148,m));
    chlaNormalizedReflectance = hoopData(:,m)./chlaTrough-1;
    
    [maxGreen(m), greenIndex(m)] = max(chlaNormalizedReflectance(76:113));
    [maxRedEdge(m),redEdgeIndex(m)] = max(chlaNormalizedReflectance(148:164));
     
    fprintf('Generating green/red edge ratio %.0f of %.0f \n', m, size(hoopData,2));
end

greenRedEdgeRatio = maxGreen./maxRedEdge;
normalizedDiff = (maxRedEdge - maxGreen) ./ (maxRedEdge + maxGreen);

greenSlope = maxGreen./( wavelengths(76+greenIndex) - wavelengths(129+chlaTroughIndex) );
redEdgeSlope = maxRedEdge./( wavelengths(148+redEdgeIndex) - wavelengths(129+chlaTroughIndex) );

testSlope = greenSlope./redEdgeSlope;

figure
plot(normalizedDiff, chlaConcentration,'o');