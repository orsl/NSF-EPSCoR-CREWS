%% Must be run after chlaRegression.m... This should be fixed

%Riley Logan
%April 2022

function [XL,YL,XS,YS,beta,PCTVAR,MSE,statsMSE] = PLSR(hoopData, chlaData)


%% Split into training and test sets 
% Cross varidation (train: 80%, test: 20%)
cv = cvpartition(size(hoopData,2),'HoldOut',0.4);
idx = cv.test;
% Separate to training and test data
dataTrain_hoops = hoopData(:,~idx);
dataTest_hoops  = hoopData(:,idx,:);

dataTrain_chla = chlaData(:,~idx); 
dataTest_chla = chlaData(:,idx);

%% Do PLSR on data (train)
X = dataTrain_hoops';
Y = dataTrain_chla';

ncomp = 6;

[XL,YL,XS,YS,beta,PCTVAR,MSE,statsMSE] = plsregress(X,Y,ncomp);

% number of components to use
%Visually:
figure
plot(1:ncomp,100*cumsum(PCTVAR(1,:)),'b-o');
xlabel('Number of Principal Components');
ylabel('Percent Variance Explained in X');
%With cross-validation:
%figure
%plot(0:ncomp,PLSmsep(2,:),'b-o');
%xlabel('Number of components');
%ylabel('Estimated Mean Squared Prediction Error');

%% Assess how we did (use test data)figure
figure
yfit = [ones(size(dataTest_hoops',1),1) dataTest_hoops']*beta;
plot(dataTest_chla', abs(yfit),'bo',1:400,1:400);
xlabel('Test Data');
ylabel('Prediction');
title('PLSR Prediction')

figure
yfit = [ones(size(X,1),1) X]*beta;
residuals = Y - yfit;
stem(residuals)
xlabel('Observations');
ylabel('Residuals');

%Calculate the normalized PLS weights
W0 = statsMSE.W ./ sqrt(sum(statsMSE.W.^2,1));

%Calculate the VIP scores for ncomp components
p = size(XL,1);
sumSq = sum(XS.^2,1).*sum(YL.^2,1);
vipScore = sqrt(p* sum(sumSq.*(W0.^2),2) ./ sum(sumSq,2));

%Find variables with a VIP score greater than or equal to 1
indVIP = find(vipScore >= 1);

%Plot the VIP scores
figure
scatter(1:length(vipScore),vipScore,'x')
hold on
scatter(indVIP,vipScore(indVIP),'rx')
plot([1 length(vipScore)],[1 1],'--k')
hold off
axis tight
xlabel('Predictor Variables')
ylabel('VIP Scores')